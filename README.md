# GEF SWS Theme
> This is the first of potentially several School Website Service themes designed for future integration with Adobe Experience Manager.
> Initially all SWS specific compontents/patterns will be developed in this theme and not in "White Label". The decission to promote a compontent to white label will be made at a later date. 

Author: Digital Servies, Communications & Engament Directorate, NSW Department of Education

### Proxy settings

If you're working at DoE, you'll need these proxy settings:

- http: http://proxy.det.nsw.edu.au:80
- https: http://proxy.det.nsw.edu.au:80

### Choose your flavour

- `$ gulp watch`       - compiles the src dir and serves the templates using browserSync
- `$ gulp build`       - simply builds the src dir into the dist dir
- `$ gulp distribute`  - simply builds the src dir into the dist dir with compression and uncss
- `$ gulp deploy`      - sends to our test environment (DoE only) & needs the `--env="string"` flag
- `$ gulp gef-update`  - updates the current repo to the latest GEF Generator Master

### Add your toppings

- `$ gulp [task] --compress`      - compresses built assets
- `$ gulp [task] --uncss`         - use uncss to strip out unused css stuff
- `$ gulp [task] --cms=matrix`    - restructures the images dir and matches CSS requirements to Matrix's asset tree
- `$ gulp deploy --env="string"`  - deploy to an environment of your choosing (employees of DoE only)