"use strict"

import * as $ from 'jquery'
import fancybox from 'fancybox'
import justifiedGallery from 'justifiedGallery'

/**
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2017 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 * @requires lightbox
 */

class albumDetailsComponent {

    /**
     * Script for Album details page
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {
        // console.log('albumDetailsComponent constructor START', config)

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }

        // VARIABLES
        this.loaderDelay = 0
        this.viewMoreContainer
        this.viewMoreButton
        this.galleryItems
        this.imageContainerHeight
        this.firstImageInArray
        this.mainContent = $('.gef-main')
        this.contentWrapper = $('#sws-album-details')
        this.contentContainer = $('.sws-album-details__container')
        this.viewMoreButtonHtml = `<button class="album-view-more sws-view-more" type="button"><h2>More images</h2></button>`
            // console.log('this.galleryItems', this.galleryItems)
        this.imageId = 0
        this.counter = 0
        this.urlCounter = 0
        this.callBackUrls = ['../assets/js/data/albumData.json', '../assets/js/data/albumData1.json', '../assets/js/data/albumData2.json']
        this.rowHeight = this.setRowHeight()
            // Load first 20 images on start up
        this.contentContainer.html(this.loader)
        this.ajaxCall(this.callBackUrls[this.urlCounter])

        // EVENT LISTENERS
        $(window).resize(() => {
            this.rowHeight = this.setRowHeight()
        })

        // console.log('albumDetailsComponent constructor END')
    }

    setRowHeight() {
        if (window.matchMedia("(max-width:768px)").matches) {
            // console.log('max-width:768px TRUE')
            return 100
        } else {
            // console.log('max-width:768px FALSE')
            return 160
        }
    }

    initGallery() {
            // console.log('justifiedGallery called')
            this.galleryItems = this.contentContainer.find('a.sws-lightbox')
            return this.contentContainer.justifiedGallery({
                margins: 5,
                rowHeight: this.rowHeight
            }).on('jg.complete', () => {
                // console.log('initGallery this.galleryItems', this.galleryItems)
                this.galleryItems.fancybox({
                    transitionEffect: 'slide',
                    infobar: true,
                    toolbar: 'auto',
                    idleTime: false,
                    buttons: [
                      "slideShow",
                      "fullScreen",
                      "thumbs",
                      "close",
                      //"download",
                      //"share",
                      // "zoom"
                    ],
                    mobile : {
                        clickContent : "close",
                        clickSlide : "close"
                    }
                })
                this.setSideButtons();
            })
        }
        //Theme 5 Image popup changes
    setSideButtons() {
            var This = this;

            $("a.sws-album-details__thumbnail").on('click', () => {
                    //Initially was build for theme-5 only
                    //if ($('body').hasClass('theme5') == true) {}
                    //changeNow();

                    let eventBtn = (event) => {
                        This.buttonCaption();
                        //off the event after first initiation
                        //prevent from multiple clicks and appends
                        $(This).off(event);
                    }
                    eventBtn();

                })
                //function to keep the nav arrows inside image
                // var changeNow = function() {
                //     let fancyBoxWidth = $('.fancybox-stage').width();
                //     $('.fancybox-inner').css('visibility', 'hidden');
                //     setTimeout(() => {
                //         let fancyImgWrap = $('.fancybox-stage').find('.fancybox-slide--current img.fancybox-image').width();
                //         let caclSideButtons = (fancyBoxWidth - fancyImgWrap) / 2;
                //         $('.fancybox-button--arrow_left').css('left', caclSideButtons);
                //         $('.fancybox-button--arrow_right').css('right', caclSideButtons);
                //         $('.fancybox-inner').css('visibility', 'visible');
                //         //console.log(caclSideButtons, ',' + fancyImgWrap);
                //         This.fancyButton(changeNow);
                //     }, 1000);
                //     This.orientCheck(changeNow);
                // }

        }
        //adjust arrow on orientation/resize window
        // orientCheck(changeNow) {
        //     window.addEventListener("resize", function() {
        //         changeNow();
        //     }, false);
        // }
        //Click on nav buttons recalculate nav button placement
        // fancyButton(changeNow) {
        //     $("button.fancybox-button").each((i, v) => {
        //         $(v).on('click', () => {
        //             changeNow();
        //         })
        //     });
        // }
    buttonCaption() {
        $('.fancybox-caption-wrap').append(`
      <div class="sws-caption-inside-wrapper">
        <div class="sws-caption-back">
          <a class="sws-caption-back__link" href="JavaScript:Void(0);">Back to album</a>
        </div>
        <div class="sws-caption-share">
          <button class="sws-caption-share__link event-details-latest-share">Share</button>
        </div>
      </div>
      `);
        $(".sws-caption-back__link").on('click', () => {
            $.fancybox.close();
        })
    }


    ajaxCall(url) {
        // console.log('| ajaxCall - url being called', url)
        $.ajax({
            url: url,
            dataType: 'json',
            complete: (data) => {
                // console.log('ajax complete', data.responseJSON)
                this.dataCall(data, this.urlCounter)
            }
        })
    }

    dataCall(data, urlCounter) {
        this.formatData(data.responseJSON, (formattedData, thereIsMoreContent, firstImage) => {
            this.ajaxCallback(formattedData, thereIsMoreContent, firstImage, urlCounter)
        })
    }

    formatData(data, callback) {
        let imagesHtml = []
        let firstImage = this.imageId
            // console.log('firstImage', firstImage)
            // create an array of the image html for rendering
        data.images.forEach((val) => {
            imagesHtml.push(
                `<a class="sws-album-details__thumbnail sws-lightbox" href="${ val.path }" data-image-id="${ val.number }" data-caption="${ val.caption }" data-fancybox="${ data.albumDetails.albumName }">
          <img src="https://placehold.it/${ val.nameThumb }" alt="${ val.number } of ${data.albumDetails.totalImages} images: ${ val.caption }">
        </a>`
            )
            this.imageId++
        })
        let thereIsMoreContent = data.albumDetails.loadMore
        callback(imagesHtml, thereIsMoreContent, firstImage)
    }

    getFirstLoadedImage(firstImage) {
        return $(`[data-image-id=${ firstImage }]`)
    }

    renderImageNodes(arr) {
        let timing = 0,
            fadeTime = 125
            // set up fade in and add all image nodes to the dom element
        for (var i = 0; i < arr.length; i++) {
            let node = $(arr[i])
            node.addClass('hidden-box')
            this.contentContainer.append(node)
            setTimeout(() => { node.removeClass('hidden-box') }, timing)
            if (timing === 0 && this.urlCounter !== 0) { setTimeout(() => { node.focus() }, fadeTime) }
            timing += fadeTime
        }

        return timing
    }

    ajaxCallback(dataArray, thereIsMoreContent, firstImage, urlCounter) {
        // if thers more content use the button else inject nothing
        let loadMoreOption = thereIsMoreContent ? this.viewMoreButtonHtml : ''
        loadMoreOption = $(loadMoreOption)
        loadMoreOption.css('opacity', 0)

        // on initial render
        if (this.urlCounter === 0) {
            let initViewMoreContainer = $(`<div id="album-view-more"></div>`)
            this.contentContainer.html('')
            let timing = this.renderImageNodes(dataArray)
            this.contentContainer.after(initViewMoreContainer.html(loadMoreOption))
            setTimeout(() => { loadMoreOption.css('opacity', 1) }, timing)
            this.galleryItems = this.contentContainer.find('a.sws-lightbox')
            this.initGallery()

            if (thereIsMoreContent) {
                this.viewMoreContainer = $('#album-view-more')
                this.viewMoreButton = $('#sws-album-details .album-view-more')
                this.buttonClickListener(this.viewMoreButton)
            }

            // on any other render
        } else {
            // settimeout emulates wait for the ajax call
            setTimeout(() => {
                this.viewMoreContainer = $('#album-view-more')
                let timing = this.renderImageNodes(dataArray)
                this.initGallery()

                // Inject html for the loadMoreOption
                this.viewMoreContainer.html(loadMoreOption)
                setTimeout(() => { loadMoreOption.css('opacity', 1) }, timing)

                if (thereIsMoreContent) {
                    this.viewMoreButton = $('#sws-album-details .album-view-more')
                    this.buttonClickListener(this.viewMoreButton)
                }
            }, this.loaderDelay)
        }
        this.urlCounter++
    }

    buttonClickListener(jqueryObj) {
        this.counter++
            return jqueryObj.click((e) => {
                e.stopPropagation()
                    // set the content wrapper min-height in order to counter the div reducing to
                    // zero height and back to full height and causing the page to jump
                this.contentWrapper.css('min-height', this.contentWrapper.height())
                this.viewMoreContainer.html(this.loader)
                let loaderContainer = this.viewMoreContainer.find('.sws-loader-svg-1').find('.show-on-sr')
                loaderContainer.focus()
                this.ajaxCall(this.callBackUrls[this.urlCounter])
            })
    }

    detectIE() {
        var ua = window.navigator.userAgent

        var msie = ua.indexOf('MSIE ')
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10)
        }

        var trident = ua.indexOf('Trident/')
        if (trident > 0) {
            // IE 11 => return version numbers
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10)
        }

        var edge = ua.indexOf('Edge/')
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10)
        }

        // other browser
        return false
    }

    loaderGifHtml() {
        // fallback gif for IE9 - 11
        return (
            `<div class="sws-loader-svg-1">
        <p tabindex="0" class="show-on-sr">Loading content</p>
        <img src="../assets/images/loader.gif">
      </div>`
        )
    }

    loaderSvgHtml() {
        // SVG for modern browswers
        return (
            `<div class="sws-loader-svg-1">
        <p tabindex="0" class="show-on-sr" aria-label="Loading content">Loading content</p>
        <?xml version="1.0" encoding="utf-8"?>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           viewBox="0 0 139 53" enable-background="new 0 0 139 53" xml:space="preserve">
           <style>.svg-rectangle { fill: rgb(100, 100, 100); }</style>
          <g id="loading-button">
            <g id="Artboard">
              <rect class="svg-rectangle" id="Rectangle" rx="1" ry="1" x="23" y="16" width="21" height="21" opacity="0">
                <animate id="anim1" attributeName="opacity" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="0;1" fill="freeze"/>
                <animate attributeName="y" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                <animate id="anim4" attributeName="opacity" attributeType="XML" begin="anim3.end" dur="0.5s" values="1;0;0" fill="freeze"/>
              </rect>
              <rect class="svg-rectangle" id="Rectangle-2" rx="1" ry="1" x="60" y="16" width="21" height="21" opacity="0">
                <animate id="anim2" attributeName="opacity" attributeType="XML" begin="anim1.end" dur="0.75s" values="0;1" fill="freeze"/>
                <animate attributeName="y" attributeType="XML" begin="anim1.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                <animate id="anim5" attributeName="opacity" attributeType="XML" begin="anim3.end + 63ms" dur="0.5s" values="1;0;0" fill="freeze"/>
              </rect>
              <rect class="svg-rectangle" id="Rectangle-3" rx="1" ry="1" x="97" y="16" width="21" height="21" opacity="0">
                <animate id="anim3" attributeName="opacity" attributeType="XML" begin="anim2.end" dur="0.75s" values="0;1" fill="freeze"/>
                <animate attributeName="y" attributeType="XML" begin="anim2.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                <animate id="anim6" attributeName="opacity" attributeType="XML" begin="anim3.end + 125ms" dur="0.5s" values="1;0;0" fill="freeze"/>
              </rect>
            </g>
          </g>
        </svg>
      </div>`
        )
    }
}

export default albumDetailsComponent
