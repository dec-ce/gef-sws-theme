"use strict"
import * as $ from "jquery"
import UI from "uikit"
/**
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class ariaToggleMenu {


    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }
        //Variables
        let container = $('.sws-mega-menu-title-bar');
        let listItem = '.list-columns';

        // --- Start observer set up ---

        // Observer constructor for aria-expanded attribute
        const searchAttrObserver = new MutationObserver((mutations) => {
            mutations.forEach((mutation) => {
                if (mutation.attributeName == 'aria-expanded') {
                    // console.log('mutation.target', mutation.target)
                    let target = $(mutation.target)[0]
                        // console.log('MutationObserver triggered, target', target)
                    this.searchAttrMutationHandler(target)
                }
            })
        })

        // get the search icon parent
        let searchIcon = document.querySelector('#sws-search-icon')

        // variable for aria expanded state
        this.ariaExpandedState = searchIcon.getAttribute('aria-expanded')

        // Add event listener and element for mutation observer
        searchAttrObserver.observe(searchIcon, { attributes: true })
        let This = this;
        // --- End observer set up ---

        // Mega menu dropdown attributes
        $('[data-uk-dropdown]').on('show.uk.dropdown', function() {
            //console.log($(this).find(listItem).length);
            if ($(this).find(listItem).length > 0 && $(this).find(listItem).children().length <= 0) {
                This.noChild($(this));
            }
            if ($(this).find(listItem).children().length <= 3)
            {
                $(this).find('.sws-toc_item').addClass('list-display');
            }
            $(this).children('a').attr("aria-expanded", "true");
            $(this).find('.sws-dropdown-menu').removeAttr("aria-hidden");
            $(this).find('.sws-dropdown-menu').attr("tabindex","0");
            removeAriaAttributes();
        });

        $('[data-uk-dropdown]').on('hide.uk.dropdown', function() {
            $(this).children('a').attr("aria-expanded", "false");
            removeAriaAttributes();
        });
        // IOS only. when touch outside the search input field will hide it
        $('body').on('touchstart',function(e) {
          if ($("#sws-search-icon-mobile").hasClass('uk-open')) {
            if((e.target.id != "search-textfield") && (e.target.id != "search-textfield__submit") && !($(e.target).hasClass("uk-icon-search")) && !($(e.target).hasClass("sws-search-icon__button")) && !($(e.target).hasClass("sws-search-icon__dropdown"))){
              $('.sws-search-icon__dropdown').hide();
              removeAriaAttributes();
              $('#sws-search-icon-mobile').removeClass('uk-open');
            }
          }
        });

        $('.sws-meganav-megalinks').on('keyup', function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 9) {
                $(this).parent().prev('.sws-parent-menu').removeClass('uk-open');
                $(this).parent().prev('.sws-parent-menu').removeAttr("aria-expanded");
                $(this).parent().prev('.sws-parent-menu').removeAttr("aria-haspopup");
                removeAriaAttributes();
            }
        });

        $("li.uk-parent.sws-parent-menu").mouseover(function() {
            setTimeout(function() {
                $(this).each(function() {
                    removeAriaAttributes();
                });
            });
        });

        function removeAriaAttributes() {
            $('li.uk-parent.sws-parent-menu').removeAttr("aria-expanded");
            $('li.uk-parent.sws-parent-menu').removeAttr("aria-haspopup");
        }

        //close search dropdown when tabbing through
        this.searchIconClass = $('.sws-search-icon')
        $('.sws-search-input__submit-button').on('keydown', (e) => {
            this.searchBoxHandler(e);
        })

         $('.sws-mega-nav-text').each(function() {
            var $this = $(this);
            $this.html($this.html().replace(/(\S+)\s*$/, '<span class="sws-mega-nav-text__content">$1</span>'));
        });

    }

    searchAttrMutationHandler(element) {
        // handle removal and addition of aria attributes
        let state = element.getAttribute('aria-expanded')
        if (state !== null) { this.ariaExpandedState = state }

        let ariaExpanded = element.getAttribute('aria-expanded')
        let ariaHaspopup = element.getAttribute('aria-haspopup')

        if (ariaExpanded) { element.removeAttribute('aria-expanded') }
        if (ariaHaspopup) { element.removeAttribute('aria-haspopup') }

        let expandButton = element.querySelector('.sws-search-icon__button')

        this.ariaExpandedState === 'true' ?
            expandButton.setAttribute('aria-label', 'Close search') :
            expandButton.setAttribute('aria-label', 'Open search')
    }
    searchBoxHandler(e) {
        var code = (e.keyCode ? e.keyCode : e.which)
        if (!e.shiftKey && code === 9) {
            this.searchIconClass.removeClass('uk-open');
            this.searchIconClass.removeAttr("aria-expanded")
            this.searchIconClass.removeAttr("aria-haspopup")
        }
    }
    noChild(el) {
        //console.log("child");
        $(el).find(".uk-dropdown.uk-dropdown-navbar").addClass('uk-hidden');
        //console.log(el);
        window.location.href = $(el).find('.sws-meganav-title').attr('href');
        //window.location.href = 'http://google.com';
    }

}

export default ariaToggleMenu
