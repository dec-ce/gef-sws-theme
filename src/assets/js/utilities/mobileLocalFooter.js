"use strict"
import * as $ from "jquery"

/**
* mobileLocalFooter.js - utility which displays accordian for mobile view
*
* @since 1.1.12
*
* @author Digital Services <communications@det.nsw.edu.au>
* @copyright © 2016 State Government of NSW 2016
*
* @class
* @requires jQuery
*/

class mobileLocalFooter {

  /**
   *
   * @param {jquery} selector - the element which is modified
   * @param {string} config.media_query - the media query which when triggers forces linking
   *
   */
  constructor(selector, config) {

    this.selector = selector
    console.log("1",this.selector )

    this.config = {
      media_query: "(min-width: 960px)"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // set the mediq query
    var media_query = window.matchMedia(this.config.media_query)
    // Test the media query
    this.testMediaQuery(media_query, selector)
  }

  /**
   * Tests the media query
   *
   * @param {string} mq - the media query string
   */
  testMediaQuery(mq, selector) {
    if (mq.matches) {
      this.changeFooter(selector)
    }
  }

  /**
   * Forces linking on click
   *
   * @param {jquery} selector - the element in question
   */
  changeFooter(selector) {
   $(selector).addClass('gef-show-hide gef-show-hide--alt uk-accordion uk-visible-small');
   $(selector).closest('h3').addClass('uk-accordion-title');
   $(selector).closest('div').addClass('uk-accordion-content');
   console.log("9",this.selector)
  }

}

export default mobileLocalFooter