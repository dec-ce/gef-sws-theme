"use strict"
import * as $ from "jquery"
    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

class ariaToggleMenu {


    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {
      console.log('tertiary nav constructor', selector)

      // Check if config has been passed to constructor
      if (config) {
          // Merge default config with passed config
          this.config = $.extend(true, {}, this.config, config)
      }

      // Observer pattern for changing aria-expanded attribute from li to child element a
      const attrObserver = this.initMutationObserver()
      // Constructor Variables
      this.el = $(selector)
      this.ariaExpandedState
      // this.expandButton = this.el.find('.sws-side__open-icon')
      this.sublistParent = selector.querySelectorAll('.sws-side__sublist-block')

      for (let i = 0; i < this.sublistParent.length; i++) {
        attrObserver.observe(this.sublistParent[i], {attributes: true})
      }

      console.log('tertiary nav constructor End')
    }

    initMutationObserver () {
      return new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          if (mutation.attributeName == 'aria-expanded') {
            let target = $(mutation.target)[0]
            this.attributeChangeHandler(target)
          }
        })
      })
    }

    attributeChangeHandler (element) {
      // Called everytime the aria-expanded attribute of li.uk-parent changes
      let el = $(element)
      let state = el.attr('aria-expanded')
      // make sure the event is valid by checking of the attribute exists
      if (typeof state !== 'undefined') {
        // remove the attribute if it exists
        el.removeAttr('aria-expanded')
        let parentTitle = el.find('.sws-side__sublist-item .sws-side__list-item span').first().text().toString()
        let expandButton = el.children('.sws-side__open-icon')
        let currentState = expandButton.attr('aria-expanded')
        let labelString = state === 'true' ? ' - sub menu open' : ' - sub menu closed'
        labelString = parentTitle + labelString

        // Add the attribute
        expandButton.attr('aria-expanded', state)
        expandButton.css("max-height", ((expandButton.prev())[0].clientHeight-7)+'px')
        expandButton.attr('aria-label', labelString)
      }
    }

}

export default ariaToggleMenu
