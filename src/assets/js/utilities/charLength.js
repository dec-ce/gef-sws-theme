"use strict"
import * as $ from "jquery"
    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

class charLength {


    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {


        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }

        var sClass = $(selector).attr('class');
        var SClassChar = sClass.split("-");
        var sClassCharLength = SClassChar[SClassChar.length - 1];

        $("." + sClass).text(function(index, currentText) {
          if (currentText){
            if (currentText.length > sClassCharLength ) {
            return (currentText.substr(0, sClassCharLength) + '...');
          }}
        });

    }

}

export default charLength