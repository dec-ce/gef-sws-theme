"use strict"
import * as $ from "jquery"
/**
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2017 State Government of NSW 2017
 *
 * @class
 * @requires jQuery
 */

class swsIESpec {

    /**
     * IE/EDGE CHECK TO INCLUDE SEPERATE CLASS TO TARGET ELEMENTS
     * CLASS: sws-ie
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {


        // Merge default config with passed config
        this.config = $.extend(true, {}, this.config, config);
        console.log("inside")
        this.ieCheck();



    }
    ieCheck() {
        var myNav = navigator.userAgent.toLowerCase();
        var findIe = myNav.indexOf('msie');
        var findEd = myNav.indexOf('edge');
        var findTr = myNav.indexOf('trident');
        // var bannerIe = $('.sws-banner-wrapper').find('img').attr('src');
        if ((findIe != -1) || (findEd != -1) || (findTr != -1)) {
            $('body').addClass('sws-ie');
            // $('.sws-banner-wrapper .sws-static-banner-component').css({ 'background-image': 'url(' + bannerIe + ')', 'background-size': 'cover', 'background-position': 'center' });
            // $('.sws-banner-wrapper .sws-static-banner-component').find('img').hide();
        }
    }
}
export default swsIESpec