"use strict"
import * as $ from "jquery"
import * as PseudoLink from "utilities/pseudoLink"
/**
 * Lazy Loading on view more click in Event Landing Page.
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class newsViewMore {


    /**
     * Generates the dropdown on button click
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     *
     */

    constructor(selector, config) {
        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }

        // VARIABLES
        this.loaderDelay = 0
        this.loader = this.detectIE() ? this.loaderGifHtml() : this.loaderSvgHtml()
        this.viewMoreContainer = $('.event-view-more ')
        this.loadBefore = $('.load-before ')
        this.viewMoreButton = $('.event-view-more .sws-view-more')
        this.contentContainer = $('.sws-news-newsContent')
        this.viewMoreButtonHtml = this.viewMoreContainer.html()
        this.counter = 0

        //PRELOAD LOADER GIF FOR IE
        if (this.detectIE()) {
            let IELoader = this.loaderGifHtml()
            let IEdiv = $('<div></div>').html(IELoader).css({ 'display': 'none', 'position': 'absolute', 'width': '0', 'height': '0' })
            $('body').append(IEdiv)
        }

        // EVENT LISTENERS
        this.buttonClickListener(this.viewMoreButton)

        console.log('loadMore constructor End')
    }

    ajaxCall() {
        $.ajax({
            url: '../assets/js/data/eventlandingData.json',
            dataType: 'json',
            complete: (data) => {
                console.log('ajax complete', data.responseJSON)
                this.formatData(data.responseJSON, (formattedData, thereIsMoreContent) => {
                    this.ajaxCallback(formattedData, thereIsMoreContent)
                })
            }
        })
    }

    formatData(data, callback) {
        let eventLandingItemsHtml = []
        data.eventLandingItems.forEach((val) => {
            let imgInclusion = ""
            if (val.pathToImage != "") {
                imgInclusion = `<img class="sws-latest-news__image" src="${val.pathToImage}" srcset="${val.pathToImage}" alt="">`
            }

            eventLandingItemsHtml.push(
                `<div class="sws-latest-news-wrapper sws-news-component-first">
                <!-- News with Image component-->
                <div class="sws-latest-news sws-latest-news--eventslanding" data-pseudo-link="data-pseudo-link">
                  ${imgInclusion}
                  <div class="sws-event-stamp-wrapper">
                  <div class="sws-single-event-square--no_twistee sws-single-event-square--eventdetails">
                    <div class="sws-single-event-square_design">
                      <div class="sws-single-event-square_inside--no_twistee">
                        <p class="sws-event-day">
                          <span class="scr-only">
                            Friday
                          </span>
                          <span aria-hidden="true">
                            Fri
                          </span>
                        </p>
                        <p class="sws-event-day-date">
                            11</p>
                        <p class="sws-event-month">
                          <span class="scr-only">
                            December
                          </span>
                          <span aria-hidden="true">
                            Dec
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class = "event-landing-article-image">
                    <article class="sws-latest-news__article"><h3>
                        <a class="sws-loaded-${val.Id}" href="${val.url}">
                        ${val.title}</a></h3>
                        <p class="sws-event-date-period"><span class="sws-event-date-period-new" aria-hidden="true"></span>${val.date}</p>
                        <p class="sws-events-heading"><span class="sws-events-heading-new" aria-hidden="true"></span>${val.time}</p>
                        <a class="sws-event-location" href="#"><span class="sws-event-location-new" aria-hidden="true"></span><span class="sws-char-length-43">${val.location}</span></a>
                        <p class="sws-char-length-1001">${val.description}</p>
                        <div class = "sws-eventlanding-cta-wrapper">
                        <div class = "sws-events-detail-header__container__txt__calplus sws-eventlanding-reminder sws-eventdetail-remindme">
                            <i class = "uk-icon-calendar-plus-o uk-margin-small-right"></i><a href="#">Remind me (ICS 12KB)</a>
                        </div>
                        <article class="sws-download-link sws-eventlanding-pdf">
                        <i class="uk-icon-paperclip" aria-hidden="true"></i>
                        <a href="${ val.pdfPath }">${ val.pdfName } (${ val.pdfSize })</a>
                        </article>
                    </div>
                    </article>
                </div>

                </div>
              </div>`
            )
        })
        let thereIsMoreContent = this.counter < 2 ? data.loadMore : !data.loadMore
        eventLandingItemsHtml = eventLandingItemsHtml.join('')
        callback(eventLandingItemsHtml, thereIsMoreContent)
    }

    ajaxCallback(formattedData, thereIsMoreContent) {
        setTimeout(() => {
            this.viewMoreContainer.fadeOut(500, () => {
                this.loadBefore.before(formattedData)
                let loadMoreOption = thereIsMoreContent ? this.viewMoreButtonHtml : ''
                this.viewMoreContainer.html(loadMoreOption)
                $('a.sws-loaded-1').focus()
                let pseudoLink = new PseudoLink("[data-pseudo-link]", { selectors: { container: "[data-pseudo-link]", anchors: 'a' } })

            }).fadeIn(500, () => {
                if (thereIsMoreContent) {
                    this.viewMoreButton = $('.event-view-more .sws-view-more')
                    this.buttonClickListener(this.viewMoreButton)
                }
            })
        }, this.loaderDelay)
    }

    buttonClickListener(jqueryObj) {
        this.counter++
            return jqueryObj.click((e) => {
                e.stopPropagation()
                this.viewMoreContainer.html(this.loader)
                let loaderContainer = this.viewMoreContainer.find('.sws-loader-svg-1').find('.show-on-sr')
                loaderContainer.focus()
                this.ajaxCall()
            })
    }

    detectIE() {
        var ua = window.navigator.userAgent

        var msie = ua.indexOf('MSIE ')
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10)
        }

        var trident = ua.indexOf('Trident/')
        if (trident > 0) {
            // IE 11 => return version numbers
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10)
        }

        var edge = ua.indexOf('Edge/')
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10)
        }

        // other browser
        return false
    }

    loaderGifHtml() {
        return (
            `<div class="sws-loader-svg-1">
          <p tabindex="0" class="show-on-sr">Loading content</p>
          <img src="../assets/images/loader.gif">
        </div>`
        )
    }

    loaderSvgHtml() {
        return (
            `<div class="sws-loader-svg-1">
          <p tabindex="0" class="show-on-sr" aria-label="Loading content">Loading content</p>
          <?xml version="1.0" encoding="utf-8"?>
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 139 53" enable-background="new 0 0 139 53" xml:space="preserve">
             <style>.svg-rectangle { fill: rgb(100, 100, 100); }</style>
            <g id="loading-button">
              <g id="Artboard">
                <rect class="svg-rectangle" id="Rectangle" rx="1" ry="1" x="23" y="16" width="21" height="21" opacity="0">
                  <animate id="anim1" attributeName="opacity" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="0;1" fill="freeze"/>
                  <animate attributeName="y" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                  <animate id="anim4" attributeName="opacity" attributeType="XML" begin="anim3.end" dur="0.5s" values="1;0;0" fill="freeze"/>
                </rect>
                <rect class="svg-rectangle" id="Rectangle-2" rx="1" ry="1" x="60" y="16" width="21" height="21" opacity="0">
                  <animate id="anim2" attributeName="opacity" attributeType="XML" begin="anim1.end" dur="0.75s" values="0;1" fill="freeze"/>
                  <animate attributeName="y" attributeType="XML" begin="anim1.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                  <animate id="anim5" attributeName="opacity" attributeType="XML" begin="anim3.end + 63ms" dur="0.5s" values="1;0;0" fill="freeze"/>
                </rect>
                <rect class="svg-rectangle" id="Rectangle-3" rx="1" ry="1" x="97" y="16" width="21" height="21" opacity="0">
                  <animate id="anim3" attributeName="opacity" attributeType="XML" begin="anim2.end" dur="0.75s" values="0;1" fill="freeze"/>
                  <animate attributeName="y" attributeType="XML" begin="anim2.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                  <animate id="anim6" attributeName="opacity" attributeType="XML" begin="anim3.end + 125ms" dur="0.5s" values="1;0;0" fill="freeze"/>
                </rect>
              </g>
            </g>
          </svg>
        </div>`
        )
    }
}

export default newsViewMore
