"use strict"
import * as $ from "jquery"
/**
 * Lazy Loading on view more click in News Landing Page.
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 * @requires jQuery
 */

class contactMap {

    /**
     * Generates the Map with campus pin pointed on the map
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    constructor(selector, config) {

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = $.extend(true, {}, this.config, config)
        }
        this.mapicon = "https://maps.google.com/mapfiles/ms/micons/red-pushpin.png";
        this.mapicon2 = "https://maps.google.com/mapfiles/ms/micons/blue-pushpin.png";
        this.infoWin = new google.maps.InfoWindow();
        this.markerList = [];

        //new instance of map
        this.map = new google.maps.Map(document.getElementById('map'), {
            //center: myLatLng,
            zoom: 18
        });
        this.loadMap();
        //console.log('Map Loaded')
    }

    loadMap() {
        //var myLatLng = { lat: -34.397, lng: 150.644 };
        var geocode = new google.maps.Geocoder();
        var items = ['#4661C4', '#AD4443'];

        //Load all the adresses from DOM
        var addressArray = this.loadAddress();
        //var pathSvg = `M298.028 214.267L285.793 96H328c13.255 0 24-10.745 24-24V24c0-13.255-10.745-24-24-24H56C42.745 0 32 10.745 32 24v48c0 13.255 10.745 24 24 24h42.207L85.972 214.267C37.465 236.82 0 277.261 0 328c0 13.255 10.745 24 24 24h136v104.007c0 1.242.289 2.467.845 3.578l24 48c2.941 5.882 11.364 5.893 14.311 0l24-48a8.008 8.008 0 0 0 .845-3.578V352h136c13.255 0 24-10.745 24-24-.001-51.183-37.983-91.42-85.973-113.733z`;
        /* var imgIcon = {
            path: pathSvg,
            fillOpacity: 1,
            scale: .05,
            strokeColor: 'black',
            strokeWeight: 1,
            rotation: 40
        }; */
        var This = this;
        var marker = [];
        var addressArrayLength = Object.keys(addressArray).length
            // console.log('addressArray', addressArray)

        $.fn.One = function() {
            $.each(addressArray, function(i, v) {
                //Get the geocode, lat lng for each address
                geocode.geocode({
                    address: v.text
                }, function(results, status) {
                    var latN = results[0].geometry.location.lat();
                    var lngN = results[0].geometry.location.lng();
                    //Create markers for each lat and lng
                    let mapObj = new google.maps.Marker({
                        position: { lat: latN, lng: lngN },
                        map: This.map,
                        animation: google.maps.Animation.DROP,
                        title: v.text,
                        icon: This.mapicon,
                        markId: v.mark,
                        headData: v.head
                    })

                    //attaching the infowindow content to the marker object itself
                    mapObj.contentInfo = This.showInfo(mapObj);
                    //adding click event listener
                    mapObj.addListener('click', function() {
                            This.markerClick(mapObj);
                        })
                        // add the mabObj to the array
                    marker.push(mapObj)
                    if (i == 0) {
                        // attaching one time dblclick listener for first marker showcase
                        google.maps.event.addListenerOnce(mapObj, 'dblclick', function() {
                            google.maps.event.trigger(marker[0], 'click');
                            $('.category-sidebar--contact-us__inner__add__block.uk-accordion-content').css('visibility', 'visible')
                        })
                    }
                    //set the center of the map to the last pinpoint
                    This.map.setCenter({ lat: latN, lng: lngN })

                    if (addressArrayLength - 1 === parseInt(i)) {
                        // console.log('reached geocode each loop end')
                        // Add the markers to the map
                        This.bounds(marker)
                    }
                });
                // console.log('$each i, v, marker', i, v, marker)
            });
            // add a listener for when the tiles have loaded and then...
            google.maps.event.addListener(This.map, 'tilesloaded', () => {
                // trigger the dropdown now that the map and markers are rendered
                // console.log('cb in bounds called 2', marker[0])
                google.maps.event.trigger(marker[0], 'dblclick')
            })
        }
        $(document).One()
        This.markerList = marker;
        this.sidebarClick();
    }

    loadAddress() {
        var addressL = {};
        var addressDiv = '.category-sidebar--contact-us__markers__address';
        var returnVal = [];
        let i = 0;
        $('.category-sidebar--contact-us__inner__add__block.uk-accordion-content').each(function() {
            let add = $(this).find(addressDiv).text();
            //setting the data marker attribute
            $(this).prev().attr('data-marker', i);
            // getting the header text for info window
            let headData = $(this).prev().find('h4').text();
            addressL[i] = { 'text': `${add}`, 'mark': `${i}`, 'head': `${headData}` };
            i++;
        });
        return addressL;

    }

    showInfo(currentMarker) {
        var This = this;
        let headLogo = $('img.local-header-logo').attr('src');
        var infobox = `<div class="uk-grid uk-grid-collapse infowindow_wrapper">
                          <img class="uk-float-left rtoicon" src="${headLogo}" alt="">
                          <div class="infotxt">
                            <strong>${currentMarker.headData}</strong><br>
                            <small>${currentMarker.title}</small><br>
                            <a href="https://www.google.com/maps/dir//${currentMarker.title}">
                              <i class="uk-icon-arrow-circle-right"></i>
                              Get Directions
                            </a>
                          </div>
                        </div>`;
        return infobox
    }

    markerClick(currentMarker) {
        var This = this;
        //expanding the current markers campus from sidebar
        $('.category-sidebar--contact-us__inner__wrapper.uk-accordion-title').each(function() {
            if ($(this).attr('data-marker') == currentMarker.markId) {
                //click causes the aria-expanded true, and set the classto uk-active
                //This causes mutation observer working, setting the infowindow and chnaging the pin marker color
                $(this).click();
            }
        })

    }

    bounds(marker) {
        var This = this;
        var bounds = new google.maps.LatLngBounds()

        // add markers to the bounds obj
        $.each(marker, function(i, v) {
            bounds.extend(marker[i].getPosition())
        })

        //get and set Zoom to beautify
        var zoom = This.map.getZoom()

        // Set bounds and zoom
        This.map.fitBounds(bounds)
        This.map.setZoom(zoom - 1)
    }

    sidebarClick() {
        var This = this;
        let parentDiv = ".category-sidebar--contact-us__inner__wrapper.uk-accordion-title";
        let openableDiv = "div[aria-expanded='true']";
        let closedDiv = "div[aria-expanded='false']";
        //Keyboard enter to click
        $(parentDiv).on('keyup', (e) => {
                if (e.keyCode === 13) {
                    e.target.click();
                }
            })
            // Options for the observer (which mutations to observe)
        var config = { attributes: true, attributeOldValue: true };
        $(parentDiv).each(function() {
            // Select the node that will be observed for mutations
            var targetNode = this
                // Callback function to execute when mutations are observed
            var callback = function(mutationsList) {
                for (var mutation of mutationsList) {
                    //console.log(mutation.target.className.indexOf("uk-active"));
                    if (mutation.type == 'attributes' && mutation.attributeName == 'class' && mutation.target.className.indexOf("uk-active") != -1) {
                        var sidebarHead = mutation.target;

                        //Rotating the Plus sign on the sidebar
                        //$(sidebarHead).find('i').css('transform', 'rotate(45deg)');

                        //Opened Div aria-hidden changes
                        $(openableDiv).attr('aria-hidden', 'false');
                        //Closed div aria-hidden changes
                        $(closedDiv).attr('aria-hidden', 'true');

                        $.each(This.markerList, function() {
                            if (mutation.target.dataset.marker == this.markId) {
                                This.infoWin.open(This.map, this);
                                This.infoWin.setContent(this.contentInfo);
                                this.setIcon(This.mapicon2);
                            } else {
                                this.setIcon(This.mapicon);
                            }
                        })
                    } else if (mutation.type == 'attributes' && mutation.attributeName == 'class' && mutation.target.className.indexOf("uk-active") == -1) {
                        var sidebarHead = mutation.target;
                        //Opened Div aria-hidden changes
                        $(openableDiv).attr('aria-hidden', 'false');
                        //Closed div aria-hidden changes
                        $(closedDiv).attr('aria-hidden', 'true');
                        //$(sidebarHead).find('i').css('transform', 'rotate(0deg)');
                        //alert('else in')
                    }
                }
            };

            // Create an observer instance linked to the callback function
            var observer = new MutationObserver(callback);

            // Start observing the target node for configured mutations
            observer.observe(targetNode, config);

            // Later, you can stop observing
            //observer.disconnect();
        })

    }
}
export default contactMap