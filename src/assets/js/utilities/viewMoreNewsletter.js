"use strict"
import * as $ from "jquery"
/**
 *
 * @since 1.0.0
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2017 State Government of NSW 2017
 *
 * @class
 * @requires jQuery
 */

class viewMore {

   /**
   * Newsletter page Ajax load on click
   *
   * @constructor
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options. Options vary depending on need
   *
   */

  constructor(selector, config) {

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // VARIABLES
    this.loaderDelay = 0
    this.loader = this.detectIE() ? this.loaderGifHtml() : this.loaderSvgHtml()
    this.viewMoreContainer = $('#newsletter-view-more')
    this.viewMoreButton = $('#newsletter-view-more .newsletter-view-more')
    this.contentContainer = $('.sws-newsletter__content')
    this.viewMoreButtonHtml = this.viewMoreContainer.html()
    this.articleId = 0
    this.counter = 0

    //PRELOAD LOADER GIF FOR IE
    if (this.detectIE()) {
      let IELoader = this.loaderGifHtml()
      let IEdiv = $('<div></div>').html(IELoader).css({'display':'none','position':'absolute','width':'0','height': '0'})
      $('body').append(IEdiv)
    }

    // EVENT LISTENERS
    this.buttonClickListener(this.viewMoreButton)

    console.log('viewMore constructor End')
  }

  ajaxCall () {
    $.ajax({
      url: '../assets/js/data/newsletterData.json',
      dataType: 'json',
      complete: (data) => {
        console.log('ajax complete', data.responseJSON)
        this.formatData(data.responseJSON, (formattedData, thereIsMoreContent, firstNewsletter) => {
          this.ajaxCallback(formattedData, thereIsMoreContent, firstNewsletter)
        })
      }
    })
  }

  formatData (data, callback) {
    let newsletterItemsHtml = []
    let firstNewsletter = this.articleId
    data.newsletterItems.forEach((val) => {
      newsletterItemsHtml.push(
        `<article class="sws-newsletter--component" data-article-id="${ this.articleId }">
          <div class="uk-width-large-1-1 uk-flex uk-flex-top uk-flex-space-between">
            <div class="uk-width-large-1-4 uk-visible-large sws-newsletter__image--wrap">
              <div class="sws-newsletter__image--a">
                <img src="https://placehold.it/140x110" alt="">
              </div>
            </div>
            <div class="uk-width-large-3-4 sws-newsletter__copy">
              <h3><a href="#">${ val.title }</a></h3>
              <p class="sws-newsletter__date">02 June 2017</p>
              <p>${ val.description }</p>
              <article class="sws-download-link">
                <i class="uk-icon-paperclip" aria-hidden="true"></i>
                <a href="${ val.pdfPath }">${ val.pdfName }
                  <span class="sws-download-link__file-size">(${ val.pdfSize })</span>
                </a>
              </article>
            </div>
          </div>
        </article>`
      )
      this.articleId++
    })
    let thereIsMoreContent = this.counter < 2 ? data.loadMore : !data.loadMore
    newsletterItemsHtml = newsletterItemsHtml.join('')
    callback(newsletterItemsHtml, thereIsMoreContent, firstNewsletter)
  }

  getFirstLoadedArticle (firstNewsletter) {
    return $(`[data-article-id="${ firstNewsletter }"]`)
  }

  ajaxCallback (formattedData, thereIsMoreContent, firstNewsletter) {
    setTimeout(() => {
      this.viewMoreContainer.fadeOut(500, () => {
        this.viewMoreContainer.before(formattedData)
        let loadMoreOption = thereIsMoreContent ? this.viewMoreButtonHtml : ''
        this.viewMoreContainer.html(loadMoreOption)
        // set focus on element
        let focusEl = this.getFirstLoadedArticle(firstNewsletter).find('.sws-newsletter__copy h3 a')
        // focusEl.attr('tabindex', '0')
        focusEl.focus()
      }).fadeIn(500, () => {
        if (thereIsMoreContent) {
          this.viewMoreButton = $('#newsletter-view-more .newsletter-view-more')
          this.buttonClickListener(this.viewMoreButton)
        }
      })
    }, this.loaderDelay)
  }

  buttonClickListener (jqueryObj) {
    this.counter++
    return jqueryObj.click((e) => {
      e.stopPropagation()
      this.viewMoreContainer.html(this.loader)
      let loaderContainer = this.viewMoreContainer.find('.sws-loader-svg-1').find('.show-on-sr')
      loaderContainer.focus()
      this.ajaxCall()
    })
  }

  detectIE () {
    var ua = window.navigator.userAgent

    var msie = ua.indexOf('MSIE ')
    if (msie > 0) {
      // IE 10 or older => return version number
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10)
    }

    var trident = ua.indexOf('Trident/')
    if (trident > 0) {
      // IE 11 => return version numbers
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10)
    }

    var edge = ua.indexOf('Edge/')
    if (edge > 0) {
     // Edge (IE 12+) => return version number
     return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10)
    }

    // other browser
    return false
  }

  loaderGifHtml () {
    return (
      `<div class="sws-loader-svg-1">
        <p tabindex="0" class="show-on-sr">Loading content</p>
        <img src="../assets/images/loader.gif">
      </div>`
    )
  }

  loaderSvgHtml () {
    return (
      `<div class="sws-loader-svg-1">
        <p tabindex="0" class="show-on-sr" aria-label="Loading content">Loading content</p>
        <?xml version="1.0" encoding="utf-8"?>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
           viewBox="0 0 139 53" enable-background="new 0 0 139 53" xml:space="preserve">
           <style>.svg-rectangle { fill: rgb(100, 100, 100); }</style>
          <g id="loading-button">
            <g id="Artboard">
              <rect class="svg-rectangle" id="Rectangle" rx="1" ry="1" x="23" y="16" width="21" height="21" opacity="0">
                <animate id="anim1" attributeName="opacity" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="0;1" fill="freeze"/>
                <animate attributeName="y" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                <animate id="anim4" attributeName="opacity" attributeType="XML" begin="anim3.end" dur="0.5s" values="1;0;0" fill="freeze"/>
              </rect>
              <rect class="svg-rectangle" id="Rectangle-2" rx="1" ry="1" x="60" y="16" width="21" height="21" opacity="0">
                <animate id="anim2" attributeName="opacity" attributeType="XML" begin="anim1.end" dur="0.75s" values="0;1" fill="freeze"/>
                <animate attributeName="y" attributeType="XML" begin="anim1.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                <animate id="anim5" attributeName="opacity" attributeType="XML" begin="anim3.end + 63ms" dur="0.5s" values="1;0;0" fill="freeze"/>
              </rect>
              <rect class="svg-rectangle" id="Rectangle-3" rx="1" ry="1" x="97" y="16" width="21" height="21" opacity="0">
                <animate id="anim3" attributeName="opacity" attributeType="XML" begin="anim2.end" dur="0.75s" values="0;1" fill="freeze"/>
                <animate attributeName="y" attributeType="XML" begin="anim2.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>
                <animate id="anim6" attributeName="opacity" attributeType="XML" begin="anim3.end + 125ms" dur="0.5s" values="1;0;0" fill="freeze"/>
              </rect>
            </g>
          </g>
        </svg>
      </div>`
    )
  }
}

export default viewMore
