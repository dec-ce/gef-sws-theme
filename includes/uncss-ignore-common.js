module.exports = function() {

    var uncssIgnoreCommon = [
        /(\.gef-tabs.+)/,
        /(\.gef-tag-list.*)/,
        /(.+\.active.*)/,
        /(.+\.uk-active.*)/,
        /(.+\[aria-expanded.*)/,
        /(.+\[aria-hidden.*)/,
        /(.+iframe.*)/,
        /(.+placeholder.*)/,
        /(\.gef-ie9.+)/,
        /(\.gef-btt.+)/,
        /(\.gef-pseudo-link)/,
        /(\.gef-breadcrumbs.+)/,
        /(\.gef-pagination.+)/,
        /(\.call-out)/,
        /(\.gef-linkgroup-list.+)/,
        /(\.gef-tile-list.+)/,
        /(\.gef-search.+)/,
        /(\.gef-external-link)/,
        /\[ng:cloak\]/,
        /\[ng-cloak\]/,
        /\[data-ng-cloak\]/,
        /\[x-ng-cloak\]/,
        /(\.ng-cloak)/,
        /(\.x-ng-cloak)/,
        /uk-icon-*/,
        /(\.gef-anchor-box)/,
        /(\.gef-link-list)/,
        /(\.uk-open.*)/,
        /(\.sws-local-mobile-nav)/,
        /(\.sws-newsletter)/,
        /(\.sws-latest-news)/,
        /(\.sws-side__sublist-block)/,
        /(\/div:last-child)/,
        /(\.sws-album-details)/,
        /(\.fancybox)/,
        /(\.sws-albums-landing)/,
        /(\#albums-landing-view-more)/,
        /(\.albums-landing-view-more)/,
        /(\.sws-events-detail__container)/,
        /(\.sws-ie)/,
        /(\#map)/,
        /(\.gef-social-media-post.*)/,
        /(\.contact_us__map.*)/

    ]

    return uncssIgnoreCommon;

}