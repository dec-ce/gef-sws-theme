define(["exports", "module", "jquery"], function (exports, module, _jquery) {
    "use strict";

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

    var charLength =

    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    function charLength(selector, config) {
        _classCallCheck(this, charLength);

        // Check if config has been passed to constructor
        if (config) {
            // Merge default config with passed config
            this.config = _jquery.extend(true, {}, this.config, config);
        }

        var sClass = (0, _jquery)(selector).attr('class');
        var SClassChar = sClass.split("-");
        var sClassCharLength = SClassChar[SClassChar.length - 1];

        (0, _jquery)("." + sClass).text(function (index, currentText) {
            if (currentText) {
                if (currentText.length > sClassCharLength) {
                    return currentText.substr(0, sClassCharLength) + '...';
                }
            }
        });
    };

    module.exports = charLength;
});