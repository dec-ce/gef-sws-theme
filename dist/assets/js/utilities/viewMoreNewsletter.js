define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2017 State Government of NSW 2017
   *
   * @class
   * @requires jQuery
   */

  var viewMore = (function () {

    /**
    * Newsletter page Ajax load on click
    *
    * @constructor
    *
    * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
    * @param {Object} config - class configuration options. Options vary depending on need
    *
    */

    function viewMore(selector, config) {
      _classCallCheck(this, viewMore);

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // VARIABLES
      this.loaderDelay = 0;
      this.loader = this.detectIE() ? this.loaderGifHtml() : this.loaderSvgHtml();
      this.viewMoreContainer = (0, _jquery)('#newsletter-view-more');
      this.viewMoreButton = (0, _jquery)('#newsletter-view-more .newsletter-view-more');
      this.contentContainer = (0, _jquery)('.sws-newsletter__content');
      this.viewMoreButtonHtml = this.viewMoreContainer.html();
      this.articleId = 0;
      this.counter = 0;

      //PRELOAD LOADER GIF FOR IE
      if (this.detectIE()) {
        var IELoader = this.loaderGifHtml();
        var IEdiv = (0, _jquery)('<div></div>').html(IELoader).css({ 'display': 'none', 'position': 'absolute', 'width': '0', 'height': '0' });
        (0, _jquery)('body').append(IEdiv);
      }

      // EVENT LISTENERS
      this.buttonClickListener(this.viewMoreButton);

      console.log('viewMore constructor End');
    }

    _createClass(viewMore, [{
      key: "ajaxCall",
      value: function ajaxCall() {
        var _this = this;

        _jquery.ajax({
          url: '../assets/js/data/newsletterData.json',
          dataType: 'json',
          complete: function complete(data) {
            console.log('ajax complete', data.responseJSON);
            _this.formatData(data.responseJSON, function (formattedData, thereIsMoreContent, firstNewsletter) {
              _this.ajaxCallback(formattedData, thereIsMoreContent, firstNewsletter);
            });
          }
        });
      }
    }, {
      key: "formatData",
      value: function formatData(data, callback) {
        var _this2 = this;

        var newsletterItemsHtml = [];
        var firstNewsletter = this.articleId;
        data.newsletterItems.forEach(function (val) {
          newsletterItemsHtml.push("<article class=\"sws-newsletter--component\" data-article-id=\"" + _this2.articleId + "\">\n          <div class=\"uk-width-large-1-1 uk-flex uk-flex-top uk-flex-space-between\">\n            <div class=\"uk-width-large-1-4 uk-visible-large sws-newsletter__image--wrap\">\n              <div class=\"sws-newsletter__image--a\">\n                <img src=\"https://placehold.it/140x110\" alt=\"\">\n              </div>\n            </div>\n            <div class=\"uk-width-large-3-4 sws-newsletter__copy\">\n              <h3><a href=\"#\">" + val.title + "</a></h3>\n              <p class=\"sws-newsletter__date\">02 June 2017</p>\n              <p>" + val.description + "</p>\n              <article class=\"sws-download-link\">\n                <i class=\"uk-icon-paperclip\" aria-hidden=\"true\"></i>\n                <a href=\"" + val.pdfPath + "\">" + val.pdfName + "\n                  <span class=\"sws-download-link__file-size\">(" + val.pdfSize + ")</span>\n                </a>\n              </article>\n            </div>\n          </div>\n        </article>");
          _this2.articleId++;
        });
        var thereIsMoreContent = this.counter < 2 ? data.loadMore : !data.loadMore;
        newsletterItemsHtml = newsletterItemsHtml.join('');
        callback(newsletterItemsHtml, thereIsMoreContent, firstNewsletter);
      }
    }, {
      key: "getFirstLoadedArticle",
      value: function getFirstLoadedArticle(firstNewsletter) {
        return (0, _jquery)("[data-article-id=\"" + firstNewsletter + "\"]");
      }
    }, {
      key: "ajaxCallback",
      value: function ajaxCallback(formattedData, thereIsMoreContent, firstNewsletter) {
        var _this3 = this;

        setTimeout(function () {
          _this3.viewMoreContainer.fadeOut(500, function () {
            _this3.viewMoreContainer.before(formattedData);
            var loadMoreOption = thereIsMoreContent ? _this3.viewMoreButtonHtml : '';
            _this3.viewMoreContainer.html(loadMoreOption);
            // set focus on element
            var focusEl = _this3.getFirstLoadedArticle(firstNewsletter).find('.sws-newsletter__copy h3 a');
            // focusEl.attr('tabindex', '0')
            focusEl.focus();
          }).fadeIn(500, function () {
            if (thereIsMoreContent) {
              _this3.viewMoreButton = (0, _jquery)('#newsletter-view-more .newsletter-view-more');
              _this3.buttonClickListener(_this3.viewMoreButton);
            }
          });
        }, this.loaderDelay);
      }
    }, {
      key: "buttonClickListener",
      value: function buttonClickListener(jqueryObj) {
        var _this4 = this;

        this.counter++;
        return jqueryObj.click(function (e) {
          e.stopPropagation();
          _this4.viewMoreContainer.html(_this4.loader);
          var loaderContainer = _this4.viewMoreContainer.find('.sws-loader-svg-1').find('.show-on-sr');
          loaderContainer.focus();
          _this4.ajaxCall();
        });
      }
    }, {
      key: "detectIE",
      value: function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          // IE 11 => return version numbers
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          // Edge (IE 12+) => return version number
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
      }
    }, {
      key: "loaderGifHtml",
      value: function loaderGifHtml() {
        return "<div class=\"sws-loader-svg-1\">\n        <p tabindex=\"0\" class=\"show-on-sr\">Loading content</p>\n        <img src=\"../assets/images/loader.gif\">\n      </div>";
      }
    }, {
      key: "loaderSvgHtml",
      value: function loaderSvgHtml() {
        return "<div class=\"sws-loader-svg-1\">\n        <p tabindex=\"0\" class=\"show-on-sr\" aria-label=\"Loading content\">Loading content</p>\n        <?xml version=\"1.0\" encoding=\"utf-8\"?>\n        <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n           viewBox=\"0 0 139 53\" enable-background=\"new 0 0 139 53\" xml:space=\"preserve\">\n           <style>.svg-rectangle { fill: rgb(100, 100, 100); }</style>\n          <g id=\"loading-button\">\n            <g id=\"Artboard\">\n              <rect class=\"svg-rectangle\" id=\"Rectangle\" rx=\"1\" ry=\"1\" x=\"23\" y=\"16\" width=\"21\" height=\"21\" opacity=\"0\">\n                <animate id=\"anim1\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"0s; anim4.end\" dur=\"0.75s\" values=\"0;1\" fill=\"freeze\"/>\n                <animate attributeName=\"y\" attributeType=\"XML\" begin=\"0s; anim4.end\" dur=\"0.75s\" values=\"20;16.5;16\" fill=\"freeze\"/>\n                <animate id=\"anim4\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim3.end\" dur=\"0.5s\" values=\"1;0;0\" fill=\"freeze\"/>\n              </rect>\n              <rect class=\"svg-rectangle\" id=\"Rectangle-2\" rx=\"1\" ry=\"1\" x=\"60\" y=\"16\" width=\"21\" height=\"21\" opacity=\"0\">\n                <animate id=\"anim2\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim1.end\" dur=\"0.75s\" values=\"0;1\" fill=\"freeze\"/>\n                <animate attributeName=\"y\" attributeType=\"XML\" begin=\"anim1.end\" dur=\"0.75s\" values=\"20;16.5;16\" fill=\"freeze\"/>\n                <animate id=\"anim5\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim3.end + 63ms\" dur=\"0.5s\" values=\"1;0;0\" fill=\"freeze\"/>\n              </rect>\n              <rect class=\"svg-rectangle\" id=\"Rectangle-3\" rx=\"1\" ry=\"1\" x=\"97\" y=\"16\" width=\"21\" height=\"21\" opacity=\"0\">\n                <animate id=\"anim3\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim2.end\" dur=\"0.75s\" values=\"0;1\" fill=\"freeze\"/>\n                <animate attributeName=\"y\" attributeType=\"XML\" begin=\"anim2.end\" dur=\"0.75s\" values=\"20;16.5;16\" fill=\"freeze\"/>\n                <animate id=\"anim6\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim3.end + 125ms\" dur=\"0.5s\" values=\"1;0;0\" fill=\"freeze\"/>\n              </rect>\n            </g>\n          </g>\n        </svg>\n      </div>";
      }
    }]);

    return viewMore;
  })();

  module.exports = viewMore;
});