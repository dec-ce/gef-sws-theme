define(['exports', 'module', 'jquery', 'fancybox', 'justifiedGallery'], function (exports, module, _jquery, _fancybox, _justifiedGallery) {
    "use strict";

    var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

    var _fancybox2 = _interopRequireDefault(_fancybox);

    var _justifiedGallery2 = _interopRequireDefault(_justifiedGallery);

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2017 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     * @requires lightbox
     */

    var albumDetailsComponent = (function () {

        /**
         * Script for Album details page
         *
         * @constructor
         *
         * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
         * @param {Object} config - class configuration options. Options vary depending on need
         *
         */

        function albumDetailsComponent(selector, config) {
            var _this = this;

            _classCallCheck(this, albumDetailsComponent);

            // console.log('albumDetailsComponent constructor START', config)

            // Check if config has been passed to constructor
            if (config) {
                // Merge default config with passed config
                this.config = _jquery.extend(true, {}, this.config, config);
            }

            // VARIABLES
            this.loaderDelay = 0;
            this.viewMoreContainer;
            this.viewMoreButton;
            this.galleryItems;
            this.imageContainerHeight;
            this.firstImageInArray;
            this.mainContent = (0, _jquery)('.gef-main');
            this.contentWrapper = (0, _jquery)('#sws-album-details');
            this.contentContainer = (0, _jquery)('.sws-album-details__container');
            this.viewMoreButtonHtml = '<button class="album-view-more sws-view-more" type="button"><h2>More images</h2></button>';
            // console.log('this.galleryItems', this.galleryItems)
            this.imageId = 0;
            this.counter = 0;
            this.urlCounter = 0;
            this.callBackUrls = ['../assets/js/data/albumData.json', '../assets/js/data/albumData1.json', '../assets/js/data/albumData2.json'];
            this.rowHeight = this.setRowHeight();
            // Load first 20 images on start up
            this.contentContainer.html(this.loader);
            this.ajaxCall(this.callBackUrls[this.urlCounter]);

            // EVENT LISTENERS
            (0, _jquery)(window).resize(function () {
                _this.rowHeight = _this.setRowHeight();
            });

            // console.log('albumDetailsComponent constructor END')
        }

        _createClass(albumDetailsComponent, [{
            key: 'setRowHeight',
            value: function setRowHeight() {
                if (window.matchMedia("(max-width:768px)").matches) {
                    // console.log('max-width:768px TRUE')
                    return 100;
                } else {
                    // console.log('max-width:768px FALSE')
                    return 160;
                }
            }
        }, {
            key: 'initGallery',
            value: function initGallery() {
                var _this2 = this;

                // console.log('justifiedGallery called')
                this.galleryItems = this.contentContainer.find('a.sws-lightbox');
                return this.contentContainer.justifiedGallery({
                    margins: 5,
                    rowHeight: this.rowHeight
                }).on('jg.complete', function () {
                    // console.log('initGallery this.galleryItems', this.galleryItems)
                    _this2.galleryItems.fancybox({
                        transitionEffect: 'slide',
                        infobar: true,
                        toolbar: 'auto',
                        idleTime: false,
                        buttons: ["slideShow", "fullScreen", "thumbs", "close"],

                        //"download",
                        //"share",
                        // "zoom"
                        mobile: {
                            clickContent: "close",
                            clickSlide: "close"
                        }
                    });
                    _this2.setSideButtons();
                });
            }

            //Theme 5 Image popup changes
        }, {
            key: 'setSideButtons',
            value: function setSideButtons() {
                var This = this;

                (0, _jquery)("a.sws-album-details__thumbnail").on('click', function () {
                    //Initially was build for theme-5 only
                    //if ($('body').hasClass('theme5') == true) {}
                    //changeNow();

                    var eventBtn = function eventBtn(event) {
                        This.buttonCaption();
                        //off the event after first initiation
                        //prevent from multiple clicks and appends
                        (0, _jquery)(This).off(event);
                    };
                    eventBtn();
                });
                //function to keep the nav arrows inside image
                // var changeNow = function() {
                //     let fancyBoxWidth = $('.fancybox-stage').width();
                //     $('.fancybox-inner').css('visibility', 'hidden');
                //     setTimeout(() => {
                //         let fancyImgWrap = $('.fancybox-stage').find('.fancybox-slide--current img.fancybox-image').width();
                //         let caclSideButtons = (fancyBoxWidth - fancyImgWrap) / 2;
                //         $('.fancybox-button--arrow_left').css('left', caclSideButtons);
                //         $('.fancybox-button--arrow_right').css('right', caclSideButtons);
                //         $('.fancybox-inner').css('visibility', 'visible');
                //         //console.log(caclSideButtons, ',' + fancyImgWrap);
                //         This.fancyButton(changeNow);
                //     }, 1000);
                //     This.orientCheck(changeNow);
                // }
            }

            //adjust arrow on orientation/resize window
            // orientCheck(changeNow) {
            //     window.addEventListener("resize", function() {
            //         changeNow();
            //     }, false);
            // }
            //Click on nav buttons recalculate nav button placement
            // fancyButton(changeNow) {
            //     $("button.fancybox-button").each((i, v) => {
            //         $(v).on('click', () => {
            //             changeNow();
            //         })
            //     });
            // }
        }, {
            key: 'buttonCaption',
            value: function buttonCaption() {
                (0, _jquery)('.fancybox-caption-wrap').append('\n      <div class="sws-caption-inside-wrapper">\n        <div class="sws-caption-back">\n          <a class="sws-caption-back__link" href="JavaScript:Void(0);">Back to album</a>\n        </div>\n        <div class="sws-caption-share">\n          <button class="sws-caption-share__link event-details-latest-share">Share</button>\n        </div>\n      </div>\n      ');
                (0, _jquery)(".sws-caption-back__link").on('click', function () {
                    _jquery.fancybox.close();
                });
            }
        }, {
            key: 'ajaxCall',
            value: function ajaxCall(url) {
                var _this3 = this;

                // console.log('| ajaxCall - url being called', url)
                _jquery.ajax({
                    url: url,
                    dataType: 'json',
                    complete: function complete(data) {
                        // console.log('ajax complete', data.responseJSON)
                        _this3.dataCall(data, _this3.urlCounter);
                    }
                });
            }
        }, {
            key: 'dataCall',
            value: function dataCall(data, urlCounter) {
                var _this4 = this;

                this.formatData(data.responseJSON, function (formattedData, thereIsMoreContent, firstImage) {
                    _this4.ajaxCallback(formattedData, thereIsMoreContent, firstImage, urlCounter);
                });
            }
        }, {
            key: 'formatData',
            value: function formatData(data, callback) {
                var _this5 = this;

                var imagesHtml = [];
                var firstImage = this.imageId;
                // console.log('firstImage', firstImage)
                // create an array of the image html for rendering
                data.images.forEach(function (val) {
                    imagesHtml.push('<a class="sws-album-details__thumbnail sws-lightbox" href="' + val.path + '" data-image-id="' + val.number + '" data-caption="' + val.caption + '" data-fancybox="' + data.albumDetails.albumName + '">\n          <img src="https://placehold.it/' + val.nameThumb + '" alt="' + val.number + ' of ' + data.albumDetails.totalImages + ' images: ' + val.caption + '">\n        </a>');
                    _this5.imageId++;
                });
                var thereIsMoreContent = data.albumDetails.loadMore;
                callback(imagesHtml, thereIsMoreContent, firstImage);
            }
        }, {
            key: 'getFirstLoadedImage',
            value: function getFirstLoadedImage(firstImage) {
                return (0, _jquery)('[data-image-id=' + firstImage + ']');
            }
        }, {
            key: 'renderImageNodes',
            value: function renderImageNodes(arr) {
                var _this6 = this;

                var timing = 0,
                    fadeTime = 125;
                // set up fade in and add all image nodes to the dom element

                var _loop = function () {
                    var node = (0, _jquery)(arr[i]);
                    node.addClass('hidden-box');
                    _this6.contentContainer.append(node);
                    setTimeout(function () {
                        node.removeClass('hidden-box');
                    }, timing);
                    if (timing === 0 && _this6.urlCounter !== 0) {
                        setTimeout(function () {
                            node.focus();
                        }, fadeTime);
                    }
                    timing += fadeTime;
                };

                for (var i = 0; i < arr.length; i++) {
                    _loop();
                }

                return timing;
            }
        }, {
            key: 'ajaxCallback',
            value: function ajaxCallback(dataArray, thereIsMoreContent, firstImage, urlCounter) {
                var _this7 = this;

                // if thers more content use the button else inject nothing
                var loadMoreOption = thereIsMoreContent ? this.viewMoreButtonHtml : '';
                loadMoreOption = (0, _jquery)(loadMoreOption);
                loadMoreOption.css('opacity', 0);

                // on initial render
                if (this.urlCounter === 0) {
                    var initViewMoreContainer = (0, _jquery)('<div id="album-view-more"></div>');
                    this.contentContainer.html('');
                    var timing = this.renderImageNodes(dataArray);
                    this.contentContainer.after(initViewMoreContainer.html(loadMoreOption));
                    setTimeout(function () {
                        loadMoreOption.css('opacity', 1);
                    }, timing);
                    this.galleryItems = this.contentContainer.find('a.sws-lightbox');
                    this.initGallery();

                    if (thereIsMoreContent) {
                        this.viewMoreContainer = (0, _jquery)('#album-view-more');
                        this.viewMoreButton = (0, _jquery)('#sws-album-details .album-view-more');
                        this.buttonClickListener(this.viewMoreButton);
                    }

                    // on any other render
                } else {
                        // settimeout emulates wait for the ajax call
                        setTimeout(function () {
                            _this7.viewMoreContainer = (0, _jquery)('#album-view-more');
                            var timing = _this7.renderImageNodes(dataArray);
                            _this7.initGallery();

                            // Inject html for the loadMoreOption
                            _this7.viewMoreContainer.html(loadMoreOption);
                            setTimeout(function () {
                                loadMoreOption.css('opacity', 1);
                            }, timing);

                            if (thereIsMoreContent) {
                                _this7.viewMoreButton = (0, _jquery)('#sws-album-details .album-view-more');
                                _this7.buttonClickListener(_this7.viewMoreButton);
                            }
                        }, this.loaderDelay);
                    }
                this.urlCounter++;
            }
        }, {
            key: 'buttonClickListener',
            value: function buttonClickListener(jqueryObj) {
                var _this8 = this;

                this.counter++;
                return jqueryObj.click(function (e) {
                    e.stopPropagation();
                    // set the content wrapper min-height in order to counter the div reducing to
                    // zero height and back to full height and causing the page to jump
                    _this8.contentWrapper.css('min-height', _this8.contentWrapper.height());
                    _this8.viewMoreContainer.html(_this8.loader);
                    var loaderContainer = _this8.viewMoreContainer.find('.sws-loader-svg-1').find('.show-on-sr');
                    loaderContainer.focus();
                    _this8.ajaxCall(_this8.callBackUrls[_this8.urlCounter]);
                });
            }
        }, {
            key: 'detectIE',
            value: function detectIE() {
                var ua = window.navigator.userAgent;

                var msie = ua.indexOf('MSIE ');
                if (msie > 0) {
                    // IE 10 or older => return version number
                    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                }

                var trident = ua.indexOf('Trident/');
                if (trident > 0) {
                    // IE 11 => return version numbers
                    var rv = ua.indexOf('rv:');
                    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                }

                var edge = ua.indexOf('Edge/');
                if (edge > 0) {
                    // Edge (IE 12+) => return version number
                    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
                }

                // other browser
                return false;
            }
        }, {
            key: 'loaderGifHtml',
            value: function loaderGifHtml() {
                // fallback gif for IE9 - 11
                return '<div class="sws-loader-svg-1">\n        <p tabindex="0" class="show-on-sr">Loading content</p>\n        <img src="../assets/images/loader.gif">\n      </div>';
            }
        }, {
            key: 'loaderSvgHtml',
            value: function loaderSvgHtml() {
                // SVG for modern browswers
                return '<div class="sws-loader-svg-1">\n        <p tabindex="0" class="show-on-sr" aria-label="Loading content">Loading content</p>\n        <?xml version="1.0" encoding="utf-8"?>\n        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\n           viewBox="0 0 139 53" enable-background="new 0 0 139 53" xml:space="preserve">\n           <style>.svg-rectangle { fill: rgb(100, 100, 100); }</style>\n          <g id="loading-button">\n            <g id="Artboard">\n              <rect class="svg-rectangle" id="Rectangle" rx="1" ry="1" x="23" y="16" width="21" height="21" opacity="0">\n                <animate id="anim1" attributeName="opacity" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="0;1" fill="freeze"/>\n                <animate attributeName="y" attributeType="XML" begin="0s; anim4.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>\n                <animate id="anim4" attributeName="opacity" attributeType="XML" begin="anim3.end" dur="0.5s" values="1;0;0" fill="freeze"/>\n              </rect>\n              <rect class="svg-rectangle" id="Rectangle-2" rx="1" ry="1" x="60" y="16" width="21" height="21" opacity="0">\n                <animate id="anim2" attributeName="opacity" attributeType="XML" begin="anim1.end" dur="0.75s" values="0;1" fill="freeze"/>\n                <animate attributeName="y" attributeType="XML" begin="anim1.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>\n                <animate id="anim5" attributeName="opacity" attributeType="XML" begin="anim3.end + 63ms" dur="0.5s" values="1;0;0" fill="freeze"/>\n              </rect>\n              <rect class="svg-rectangle" id="Rectangle-3" rx="1" ry="1" x="97" y="16" width="21" height="21" opacity="0">\n                <animate id="anim3" attributeName="opacity" attributeType="XML" begin="anim2.end" dur="0.75s" values="0;1" fill="freeze"/>\n                <animate attributeName="y" attributeType="XML" begin="anim2.end" dur="0.75s" values="20;16.5;16" fill="freeze"/>\n                <animate id="anim6" attributeName="opacity" attributeType="XML" begin="anim3.end + 125ms" dur="0.5s" values="1;0;0" fill="freeze"/>\n              </rect>\n            </g>\n          </g>\n        </svg>\n      </div>';
            }
        }]);

        return albumDetailsComponent;
    })();

    module.exports = albumDetailsComponent;
});