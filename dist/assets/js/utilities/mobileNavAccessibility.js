define(["exports", "module", "jquery", "uikit"], function (exports, module, _jquery, _uikit) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _UI = _interopRequireDefault(_uikit);

  /**
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var mobileNavAccessibility = (function () {

    /**
    * Script for Google translate feature
    *
    * @constructor
    *
    * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
    * @param {Object} config - class configuration options. Options vary depending on need
    *
    */

    function mobileNavAccessibility(selector, config) {
      var _this = this;

      _classCallCheck(this, mobileNavAccessibility);

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Observer pattern for changing aria-expanded attribute from li to child element a
      var attrObserver = this.setAttrObserver();
      // Observer for setting class on close button
      var closeLinkObserver = this.setMutationObserver();
      // Observer for search button toggle aria attributes
      var searchAttrObserver = this.setSearchMutationObserver();

      // Constructor Variables
      this.navElement = (0, _jquery)(selector);
      this.offCanvasContainer = (0, _jquery)('.uk-offcanvas-bar.sws-offcanvas-bar');
      this.offCanvasContainerJs = document.querySelector('#mobile-nav-container.uk-offcanvas-bar.sws-offcanvas-bar');
      this.mobileNavButton = (0, _jquery)('.sws-local-mobile-nav__icon--nav');
      this.closeMenuWrap = (0, _jquery)('.sws-local-mobile-nav__close');
      this.closeMenu = (0, _jquery)('.sws-local-mobile-nav__close--button');
      this.closeMenuHidden = (0, _jquery)('.sws-local-mobile-nav__close--button-hidden');
      this.expandElements = document.querySelector('.sws-local-mobile-nav__li-menu-items.uk-parent');
      this.parentEls = document.querySelectorAll('.sws-local-mobile-nav__li-menu-items.uk-parent');
      this.mobileHead = (0, _jquery)('#mobile-nav');
      this.mobileMenuIsActive = false;
      this.focusableItems = (0, _jquery)('body a:not(.sws-offcanvas-bar__header):not(.sws-offcanvas-bar__quicklinks-a):not(.sws-local-mobile-nav__li--name-a):not(.sws-local-mobile-nav__plus-icon), body input, body iframe, body button:not(.sws-local-mobile-nav__close--button):not(.sws-local-mobile-nav__close--button-hidden)');
      this.allSRItems = (0, _jquery)('body').children(':not(#mobile-nav-wrapper)');
      this.allMenuLinks = (0, _jquery)('.uk-offcanvas-bar.sws-offcanvas-bar a');
      this.lastMenuLink = (0, _jquery)('.sws-offcanvas-bar__main-unordered-list > li.sws-local-mobile-nav__li-menu-items:last-child > a.sws-local-mobile-nav__plus-icon');
      this.lastMenuLinkJs = document.querySelector('.sws-offcanvas-bar__main-unordered-list > li.sws-local-mobile-nav__li-menu-items:last-child');
      this.lastMenuHasSubMenu = true;
      this.body = (0, _jquery)('body');
      if (!(this.lastMenuLink.length > 0)) {
        this.lastMenuLink = (0, _jquery)('.sws-offcanvas-bar__main-unordered-list > li.sws-local-mobile-nav__li-menu-items:last-child > .sws-local-mobile-nav__li--name > a.sws-local-mobile-nav__li--name-a');
        this.lastMenuHasSubMenu = false;
      }
      this.headerclass = (0, _jquery)('.sws-global-header');
      // get the search icon parent
      var searchIcon = document.querySelector('#sws-search-icon-mobile');

      // variable for aria expanded state
      this.ariaExpandedState = searchIcon.getAttribute('aria-expanded');

      // Add event listener and element for mutation observer
      searchAttrObserver.observe(searchIcon, { attributes: true });

      // Event listeners
      this.navElement.on('show.uk.offcanvas', function () {
        _this.showOffCanvas();
      });
      this.navElement.on('hide.uk.offcanvas', function () {
        _this.hideOffCanvas();
      });
      this.closeMenu.click(function () {
        UIkit.offcanvas.hide();
      });
      this.closeMenuHidden.click(function () {
        UIkit.offcanvas.hide();
      });

      // Subscribe to MutationObserver targeting li.uk-parent elements
      for (var i = 0; i < this.parentEls.length; i++) {
        attrObserver.observe(this.parentEls[i], { attributes: true });
      }

      closeLinkObserver.observe(this.offCanvasContainerJs, { attributes: true });
      // console.log('End of mobileNavAccessibility constructor')
    }

    _createClass(mobileNavAccessibility, [{
      key: "setMutationObserver",
      value: function setMutationObserver() {
        var _this2 = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName == 'class') {
              if (mutation.target.classList.contains('uk-offcanvas-bar-show')) {
                _this2.closeMenuWrap.addClass('button-active');
              } else {
                _this2.closeMenuWrap.removeClass('button-active');
              }
            }
          });
        });
      }
    }, {
      key: "setAttrObserver",
      value: function setAttrObserver() {
        var _this3 = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName == 'aria-expanded') {
              var target = (0, _jquery)(mutation.target)[0];
              _this3.attributeChangeHandler(target);
            }
          });
        });
      }
    }, {
      key: "setSearchMutationObserver",
      value: function setSearchMutationObserver() {
        var _this4 = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName == 'aria-expanded') {
              // console.log('mutation.target', mutation.target)
              var target = (0, _jquery)(mutation.target)[0];
              // console.log('MutationObserver triggered, target', target)
              _this4.searchAttrMutationHandler(target);
            }
          });
        });
      }
    }, {
      key: "closeMenuClassCheck",
      value: function closeMenuClassCheck(bool) {
        var hasClass = this.closeMenuWrap.hasClass('button-active');
        if (bool) {
          if (!hasClass) {
            this.closeMenuWrap.addClass('button-active');
          }
        } else {
          if (hasClass) {
            this.closeMenuWrap.removeClass('button-active');
          }
        }
      }
    }, {
      key: "showOffCanvas",
      value: function showOffCanvas() {
        var _this5 = this;

        this.toggleAriaExpanded(this.mobileNavButton, true);
        this.closeMenuClassCheck(true);
        this.headerclass.addClass('display-hide');
        this.body.addClass("sws-local-mobile-nav__open");
        this.lastMenuLink.on('keydown', function (e) {
          if (e.key === 'Tab' && !e.shiftKey) {
            _this5.moveFocusToHead();
          }
        });

        this.closeMenu.on('keydown', function (e) {
          if (e.key === 'Tab' && e.shiftKey) {
            _this5.lastMenuLink.focus();
            e.stopPropagation();
            setTimeout(function () {
              _this5.lastMenuLink.focus();
            });
          }
        });
      }
    }, {
      key: "hideOffCanvas",
      value: function hideOffCanvas() {
        this.toggleAriaExpanded(this.mobileNavButton, false);
        this.closeMenuClassCheck(false);
        this.lastMenuLink.off();
        this.closeMenu.off();
        this.headerclass.removeClass('display-hide');
        this.body.removeClass("sws-local-mobile-nav__open");
      }
    }, {
      key: "toggleAriaExpanded",
      value: function toggleAriaExpanded(element, bool) {
        (0, _jquery)(element).attr('aria-expanded', bool);

        // If Nav is expanded
        if (bool) {
          // shift focus to the Nav header
          this.mobileMenuIsActive = true;
          this.moveFocusToHead();
          var focused = document.activeElement;
          this.toggleTabindex('-1');
          this.setAriaHiddenLabel(this.mobileMenuIsActive);
        } else {
          // remove focus from the Nav header
          this.mobileMenuIsActive = false;
          this.mobileHead.attr('tabindex', '-1');
          this.toggleTabindex('0');
          this.setAriaHiddenLabel(this.mobileMenuIsActive);
        }
      }
    }, {
      key: "setAriaHiddenLabel",
      value: function setAriaHiddenLabel(bool) {
        if (bool) {
          this.allSRItems.each(function () {
            (0, _jquery)(this).attr('aria-hidden', bool);
          });
        } else {
          this.allSRItems.each(function () {
            (0, _jquery)(this).removeAttr('aria-hidden');
          });
        }
      }
    }, {
      key: "toggleTabindex",
      value: function toggleTabindex(tabIndex) {
        if (tabIndex === '-1') {
          this.focusableItems.each(function () {
            (0, _jquery)(this).attr('tabindex', tabIndex);
          });
        } else {
          this.focusableItems.each(function () {
            (0, _jquery)(this).removeAttr('tabindex');
          });
        }
      }
    }, {
      key: "moveFocusToHead",
      value: function moveFocusToHead() {
        this.closeMenu.attr('tabindex', '0');
        this.closeMenu.focus();
      }
    }, {
      key: "attributeChangeHandler",
      value: function attributeChangeHandler(element) {
        // Called everytime the aria-expanded attribute of li.uk-parent changes
        var el = (0, _jquery)(element);
        var state = el.attr('aria-expanded');
        // make sure the event is valid by checking of the attribute exists
        if (typeof state !== 'undefined') {
          // remove the attribute if it exists
          el.removeAttr('aria-expanded');
          var parentTitle = el.find('.sws-local-mobile-nav__li--name-a').first().text().toString();
          var expandButton = el.children('.sws-local-mobile-nav__plus-icon');
          var currentState = expandButton.attr('aria-expanded');
          var labelString = state === 'true' ? ' - sub menu open' : ' - sub menu closed';
          labelString = parentTitle + labelString;

          // Add the attribute
          expandButton.attr('aria-expanded', state);
          expandButton.attr('aria-label', labelString);
        }
      }
    }, {
      key: "searchAttrMutationHandler",
      value: function searchAttrMutationHandler(element) {
        // handle removal and addition of aria attributes
        var state = element.getAttribute('aria-expanded');
        if (state !== null) {
          this.ariaExpandedState = state;
        }

        var ariaExpanded = element.getAttribute('aria-expanded');
        var ariaHaspopup = element.getAttribute('aria-haspopup');

        if (ariaExpanded) {
          element.removeAttribute('aria-expanded');
        }
        if (ariaHaspopup) {
          element.removeAttribute('aria-haspopup');
        }

        var expandButton = element.querySelector('.sws-search-icon__button');

        this.ariaExpandedState === 'true' ? expandButton.setAttribute('aria-label', 'Close search') : expandButton.setAttribute('aria-label', 'Open search');
        this.ariaExpandedState === 'true' ? expandButton.setAttribute('aria-expanded', 'true') : expandButton.setAttribute('aria-expanded', 'false');
      }
    }]);

    return mobileNavAccessibility;
  })();

  module.exports = mobileNavAccessibility;
});