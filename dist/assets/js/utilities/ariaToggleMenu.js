define(["exports", "module", "jquery", "uikit"], function (exports, module, _jquery, _uikit) {
    "use strict";

    var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    var _UI = _interopRequireDefault(_uikit);

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

    var ariaToggleMenu = (function () {

        /**
         * Script for Google translate feature
         *
         * @constructor
         *
         * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
         * @param {Object} config - class configuration options. Options vary depending on need
         *
         */

        function ariaToggleMenu(selector, config) {
            var _this = this;

            _classCallCheck(this, ariaToggleMenu);

            // Check if config has been passed to constructor
            if (config) {
                // Merge default config with passed config
                this.config = _jquery.extend(true, {}, this.config, config);
            }
            //Variables
            var container = (0, _jquery)('.sws-mega-menu-title-bar');
            var listItem = '.list-columns';

            // --- Start observer set up ---

            // Observer constructor for aria-expanded attribute
            var searchAttrObserver = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    if (mutation.attributeName == 'aria-expanded') {
                        // console.log('mutation.target', mutation.target)
                        var target = (0, _jquery)(mutation.target)[0];
                        // console.log('MutationObserver triggered, target', target)
                        _this.searchAttrMutationHandler(target);
                    }
                });
            });

            // get the search icon parent
            var searchIcon = document.querySelector('#sws-search-icon');

            // variable for aria expanded state
            this.ariaExpandedState = searchIcon.getAttribute('aria-expanded');

            // Add event listener and element for mutation observer
            searchAttrObserver.observe(searchIcon, { attributes: true });
            var This = this;
            // --- End observer set up ---

            // Mega menu dropdown attributes
            (0, _jquery)('[data-uk-dropdown]').on('show.uk.dropdown', function () {
                //console.log($(this).find(listItem).length);
                if ((0, _jquery)(this).find(listItem).length > 0 && (0, _jquery)(this).find(listItem).children().length <= 0) {
                    This.noChild((0, _jquery)(this));
                }
                if ((0, _jquery)(this).find(listItem).children().length <= 3) {
                    (0, _jquery)(this).find('.sws-toc_item').addClass('list-display');
                }
                (0, _jquery)(this).children('a').attr("aria-expanded", "true");
                (0, _jquery)(this).find('.sws-dropdown-menu').removeAttr("aria-hidden");
                (0, _jquery)(this).find('.sws-dropdown-menu').attr("tabindex", "0");
                removeAriaAttributes();
            });

            (0, _jquery)('[data-uk-dropdown]').on('hide.uk.dropdown', function () {
                (0, _jquery)(this).children('a').attr("aria-expanded", "false");
                removeAriaAttributes();
            });
            // IOS only. when touch outside the search input field will hide it
            (0, _jquery)('body').on('touchstart', function (e) {
                if ((0, _jquery)("#sws-search-icon-mobile").hasClass('uk-open')) {
                    if (e.target.id != "search-textfield" && e.target.id != "search-textfield__submit" && !(0, _jquery)(e.target).hasClass("uk-icon-search") && !(0, _jquery)(e.target).hasClass("sws-search-icon__button") && !(0, _jquery)(e.target).hasClass("sws-search-icon__dropdown")) {
                        (0, _jquery)('.sws-search-icon__dropdown').hide();
                        removeAriaAttributes();
                        (0, _jquery)('#sws-search-icon-mobile').removeClass('uk-open');
                    }
                }
            });

            (0, _jquery)('.sws-meganav-megalinks').on('keyup', function (e) {
                var code = e.keyCode ? e.keyCode : e.which;
                if (code == 9) {
                    (0, _jquery)(this).parent().prev('.sws-parent-menu').removeClass('uk-open');
                    (0, _jquery)(this).parent().prev('.sws-parent-menu').removeAttr("aria-expanded");
                    (0, _jquery)(this).parent().prev('.sws-parent-menu').removeAttr("aria-haspopup");
                    removeAriaAttributes();
                }
            });

            (0, _jquery)("li.uk-parent.sws-parent-menu").mouseover(function () {
                setTimeout(function () {
                    (0, _jquery)(this).each(function () {
                        removeAriaAttributes();
                    });
                });
            });

            function removeAriaAttributes() {
                (0, _jquery)('li.uk-parent.sws-parent-menu').removeAttr("aria-expanded");
                (0, _jquery)('li.uk-parent.sws-parent-menu').removeAttr("aria-haspopup");
            }

            //close search dropdown when tabbing through
            this.searchIconClass = (0, _jquery)('.sws-search-icon');
            (0, _jquery)('.sws-search-input__submit-button').on('keydown', function (e) {
                _this.searchBoxHandler(e);
            });

            (0, _jquery)('.sws-mega-nav-text').each(function () {
                var $this = (0, _jquery)(this);
                $this.html($this.html().replace(/(\S+)\s*$/, '<span class="sws-mega-nav-text__content">$1</span>'));
            });
        }

        _createClass(ariaToggleMenu, [{
            key: "searchAttrMutationHandler",
            value: function searchAttrMutationHandler(element) {
                // handle removal and addition of aria attributes
                var state = element.getAttribute('aria-expanded');
                if (state !== null) {
                    this.ariaExpandedState = state;
                }

                var ariaExpanded = element.getAttribute('aria-expanded');
                var ariaHaspopup = element.getAttribute('aria-haspopup');

                if (ariaExpanded) {
                    element.removeAttribute('aria-expanded');
                }
                if (ariaHaspopup) {
                    element.removeAttribute('aria-haspopup');
                }

                var expandButton = element.querySelector('.sws-search-icon__button');

                this.ariaExpandedState === 'true' ? expandButton.setAttribute('aria-label', 'Close search') : expandButton.setAttribute('aria-label', 'Open search');
            }
        }, {
            key: "searchBoxHandler",
            value: function searchBoxHandler(e) {
                var code = e.keyCode ? e.keyCode : e.which;
                if (!e.shiftKey && code === 9) {
                    this.searchIconClass.removeClass('uk-open');
                    this.searchIconClass.removeAttr("aria-expanded");
                    this.searchIconClass.removeAttr("aria-haspopup");
                }
            }
        }, {
            key: "noChild",
            value: function noChild(el) {
                //console.log("child");
                (0, _jquery)(el).find(".uk-dropdown.uk-dropdown-navbar").addClass('uk-hidden');
                //console.log(el);
                window.location.href = (0, _jquery)(el).find('.sws-meganav-title').attr('href');
                //window.location.href = 'http://google.com';
            }
        }]);

        return ariaToggleMenu;
    })();

    module.exports = ariaToggleMenu;
});