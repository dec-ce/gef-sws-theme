define(['exports', 'module'], function (exports, module) {
  "use strict";

  /**
   * Allows for basic cookie manipulation
   *
   * @since 1.1.2
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2017 State Government of NSW 2017
   *
   * @class
   */

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

  var CookieControl = (function () {

    /**
     * @constructor
     */

    function CookieControl(selector) {
      _classCallCheck(this, CookieControl);

      // gets all the cookies
      this.allCookiesString = this.getCookies();
    }

    /**
    * Exports the Class Toggle class as a module
    * @module
    */

    /**
     * @getCookies - returns the cookies
     *
     * @returns {Object}
     */

    _createClass(CookieControl, [{
      key: 'getCookies',
      value: function getCookies() {
        return document.cookie;
      }

      /**
       * @testName - tests the name as a string
       *
       * @param {string} name - name of the cookie to be modified
       *
       * @returns {true|false}
       */
    }, {
      key: 'testName',
      value: function testName(name) {
        if (name === undefined || typeof name !== 'string') {
          console.error('CookieControl: you must pass a name (string) for the cookie');
          return false;
        } else {
          return true;
        }
      }

      /**
       * @returnCookie - returns the value of the cookie
       *
       * @param {string} name - name of the cookie to be returned
       *
       * @returns {this.cookieValue|false}
       */
    }, {
      key: 'returnCookie',
      value: function returnCookie(name) {
        // test the name param
        if (!this.testName(name)) {
          return false;
        }

        var cookies = this.getCookies();
        // taken from http://stackoverflow.com/questions/10730362/get-cookie-by-name
        var value = "; " + cookies;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) {
          return parts.pop().split(";").shift();
        } else {
          this.cookieApplied = false;
          return false;
        }
      }

      /**
       * @setCookie - set the value and max_age of the cookie
       *
       * @param {string} name - name of the cookie to be returned
       * @param {string} value - the value of the cookie to be set
       * @param {string} max_age - the amount of time in seconds the cookie should remain in the user's browser
       *
       * @returns {true|false}
       */
    }, {
      key: 'setCookie',
      value: function setCookie(name, value, max_age) {
        // test the name param
        if (!this.testName(name)) {
          return false;
        }

        var expiry = ";max-age=" + max_age,
            the_string = name + "=" + value + expiry + ";path=/";
        // set the cookie
        document.cookie = the_string;
        this.cookieApplied = true;
      }
    }]);

    return CookieControl;
  })();

  module.exports = CookieControl;
});