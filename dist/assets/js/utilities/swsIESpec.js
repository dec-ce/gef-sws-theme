define(["exports", "module", "jquery"], function (exports, module, _jquery) {
    "use strict";

    var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2017 State Government of NSW 2017
     *
     * @class
     * @requires jQuery
     */

    var swsIESpec = (function () {

        /**
         * IE/EDGE CHECK TO INCLUDE SEPERATE CLASS TO TARGET ELEMENTS
         * CLASS: sws-ie
         *
         * @constructor
         *
         * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
         * @param {Object} config - class configuration options. Options vary depending on need
         *
         */

        function swsIESpec(selector, config) {
            _classCallCheck(this, swsIESpec);

            // Merge default config with passed config
            this.config = _jquery.extend(true, {}, this.config, config);
            console.log("inside");
            this.ieCheck();
        }

        _createClass(swsIESpec, [{
            key: "ieCheck",
            value: function ieCheck() {
                var myNav = navigator.userAgent.toLowerCase();
                var findIe = myNav.indexOf('msie');
                var findEd = myNav.indexOf('edge');
                var findTr = myNav.indexOf('trident');
                // var bannerIe = $('.sws-banner-wrapper').find('img').attr('src');
                if (findIe != -1 || findEd != -1 || findTr != -1) {
                    (0, _jquery)('body').addClass('sws-ie');
                    // $('.sws-banner-wrapper .sws-static-banner-component').css({ 'background-image': 'url(' + bannerIe + ')', 'background-size': 'cover', 'background-position': 'center' });
                    // $('.sws-banner-wrapper .sws-static-banner-component').find('img').hide();
                }
            }
        }]);

        return swsIESpec;
    })();

    module.exports = swsIESpec;
});