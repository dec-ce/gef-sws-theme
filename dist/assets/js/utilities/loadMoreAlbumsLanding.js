define(["exports", "module", "jquery"], function (exports, module, _jquery) {
    "use strict";

    var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

    function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2017 State Government of NSW 2017
     *
     * @class
     * @requires jQuery
     */

    var loadMoreAlbumsLanding = (function () {

        /**
         * Newsletter page Ajax load on click
         *
         * @constructor
         *
         * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
         * @param {Object} config - class configuration options. Options vary depending on need
         *
         */

        function loadMoreAlbumsLanding(selector, config) {
            _classCallCheck(this, loadMoreAlbumsLanding);

            console.log('loadMoreAlbumsLanding constructor');
            // Check if config has been passed to constructor
            if (config) {
                // Merge default config with passed config
                this.config = _jquery.extend(true, {}, this.config, config);
            }

            // VARIABLES
            this.loaderDelay = 0;
            this.loader = this.detectIE() ? this.loaderGifHtml() : this.loaderSvgHtml();
            this.viewMoreContainer = (0, _jquery)('#albums-landing-view-more');
            this.viewMoreButton = (0, _jquery)('#albums-landing-view-more .albums-landing-view-more');
            this.contentContainer = (0, _jquery)('.sws-albums-landing__content');
            this.viewMoreButtonHtml = this.viewMoreContainer.html();
            this.articleId = 0;
            this.counter = 0;

            //PRELOAD LOADER GIF FOR IE
            if (this.detectIE()) {
                var IELoader = this.loaderGifHtml();
                var IEdiv = (0, _jquery)('<div></div>').html(IELoader).css({ 'display': 'none', 'position': 'absolute', 'width': '0', 'height': '0' });
                (0, _jquery)('body').append(IEdiv);
            }

            // EVENT LISTENERS
            this.buttonClickListener(this.viewMoreButton);

            console.log('loadMoreAlbumsLanding constructor End');
        }

        _createClass(loadMoreAlbumsLanding, [{
            key: "ajaxCall",
            value: function ajaxCall() {
                var _this = this;

                _jquery.ajax({
                    url: '../assets/js/data/albumsLandingData.json',
                    dataType: 'json',
                    complete: function complete(data) {
                        console.log('ajax complete', data.responseJSON);
                        _this.formatData(data.responseJSON, function (formattedData, thereIsMoreContent, firstAlbumsLanding) {
                            _this.ajaxCallback(formattedData, thereIsMoreContent, firstAlbumsLanding);
                        });
                    }
                });
            }
        }, {
            key: "formatData",
            value: function formatData(data, callback) {
                var _this2 = this;

                var albumLandingItemsHtml = [];
                var firstAlbumsLanding = data.newsletterItems[0].id;
                data.newsletterItems.forEach(function (val) {
                    albumLandingItemsHtml.push("\n        <div class=\"sws-albums-gallery-wrapper uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1\" data-album-id=\"" + val.id + "\">\n          <div class=\"sws-albums-gallery-wrapper__inner\">\n            <a class=\"sws-albums-gallery__main-link\" href=\"" + val.urlPath + "\">\n              <div class=\"sws-albums-gallery-full-info\">\n                <p>" + val.name + "</p>\n                <div class=\"uk-grid uk-grid-collapse sws-albums-gallery-full-info__bottom\">\n                  <div class=\"uk-width-1-1 sws-albums-gallery-full-info__bottom__inner\"><img src=\"../assets/images/album_gallery_icon.svg\" srcset=\"../assets/images/album_gallery_icon.svg\" alt=\"\"><span>4</span></div>\n                </div>\n                <div class=\"sws-albums-gallery-full-info__inner-design\"></div>\n              </div>\n              <div class=\"sws-albums-gallery\">\n                <img class=\"sws-albums-gallery__image\" src=\"" + val.imgSrc + "\" srcset=\"//placehold.it/300x200\" alt=\"\">\n                <div class=\"sws-albums-gallery__album-info\">\n\n                    <div class=\"uk-grid uk-grid-collapse\">\n                      <div class=\"uk-width-1-1\">\n                        <div class=\"sws-albums-gallery__left-content\">\n                          <p class=\"sws-albums-gallery__text\">" + val.name + "</p>\n                        </div>\n                      </div>\n                      <div class=\"uk-width-4-5\">\n                        <p class=\"sws-albums-gallery__left-content\">" + val.date + "</p>\n                      </div>\n                      <div class=\"uk-width-1-5 sws-albums-gallery__right-content\">\n                        <img src=\"../assets/images/album_gallery_icon.svg\" srcset=\"../assets/images/album_gallery_icon.svg\" alt=\"\" aria-hidden=\"true\">\n                        <span aria-hidden=\"true\">" + val.totalImages + "</span>\n                        <span class=\"show-on-sr\">" + val.totalImages + " images in this album</span>\n                      </div>\n                    </div>\n\n                </div>\n              </div>\n            </a>\n          </div>\n        </div>\n        ");
                    _this2.articleId++;
                });
                var thereIsMoreContent = this.counter < 2 ? data.loadMore : !data.loadMore;
                albumLandingItemsHtml = albumLandingItemsHtml.join('');
                callback(albumLandingItemsHtml, thereIsMoreContent, firstAlbumsLanding);
            }
        }, {
            key: "getFirstLoadedArticle",
            value: function getFirstLoadedArticle(firstAlbumsLanding) {
                return (0, _jquery)("[data-album-id=\"" + firstAlbumsLanding + "\"]");
            }
        }, {
            key: "ajaxCallback",
            value: function ajaxCallback(formattedData, thereIsMoreContent, firstAlbumsLanding) {
                var _this3 = this;

                var loadMoreOption = thereIsMoreContent ? this.viewMoreButtonHtml : '';

                // image render delayed in order to emaulate ping back to server
                setTimeout(function () {
                    _this3.viewMoreContainer.fadeOut(500, function () {
                        _this3.viewMoreContainer.before(formattedData);
                        _this3.viewMoreContainer.html(loadMoreOption);
                        var focusEl = _this3.getFirstLoadedArticle(firstAlbumsLanding).find('.sws-albums-gallery__main-link');
                        focusEl.attr('tabindex', '0');
                        console.log('focusEl', focusEl);
                        focusEl.focus();
                    }).fadeIn(500, function () {
                        if (thereIsMoreContent) {
                            _this3.viewMoreButton = (0, _jquery)('#albums-landing-view-more .albums-landing-view-more');
                            _this3.buttonClickListener(_this3.viewMoreButton);
                        }
                    });
                }, this.loaderDelay);
            }
        }, {
            key: "buttonClickListener",
            value: function buttonClickListener(jqueryObj) {
                var _this4 = this;

                this.counter++;
                return jqueryObj.click(function (e) {
                    e.stopPropagation();
                    _this4.viewMoreContainer.html(_this4.loader);
                    var loaderContainer = _this4.viewMoreContainer.find('.sws-loader-svg-1').find('.show-on-sr');
                    loaderContainer.focus();
                    _this4.ajaxCall();
                });
            }
        }, {
            key: "detectIE",
            value: function detectIE() {
                var ua = window.navigator.userAgent;

                var msie = ua.indexOf('MSIE ');
                if (msie > 0) {
                    // IE 10 or older => return version number
                    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                }

                var trident = ua.indexOf('Trident/');
                if (trident > 0) {
                    // IE 11 => return version numbers
                    var rv = ua.indexOf('rv:');
                    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                }

                var edge = ua.indexOf('Edge/');
                if (edge > 0) {
                    // Edge (IE 12+) => return version number
                    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
                }

                // other browser
                return false;
            }
        }, {
            key: "loaderGifHtml",
            value: function loaderGifHtml() {
                return "<div class=\"sws-loader-svg-1\">\n        <p tabindex=\"0\" class=\"show-on-sr\">Loading content</p>\n        <img src=\"../assets/images/loader.gif\">\n      </div>";
            }
        }, {
            key: "loaderSvgHtml",
            value: function loaderSvgHtml() {
                return "<div class=\"sws-loader-svg-1\">\n        <p tabindex=\"0\" class=\"show-on-sr\" aria-label=\"Loading content\">Loading content</p>\n        <?xml version=\"1.0\" encoding=\"utf-8\"?>\n        <svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" x=\"0px\" y=\"0px\"\n           viewBox=\"0 0 139 53\" enable-background=\"new 0 0 139 53\" xml:space=\"preserve\">\n           <style>.svg-rectangle { fill: rgb(100, 100, 100); }</style>\n          <g id=\"loading-button\">\n            <g id=\"Artboard\">\n              <rect class=\"svg-rectangle\" id=\"Rectangle\" rx=\"1\" ry=\"1\" x=\"23\" y=\"16\" width=\"21\" height=\"21\" opacity=\"0\">\n                <animate id=\"anim1\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"0s; anim4.end\" dur=\"0.75s\" values=\"0;1\" fill=\"freeze\"/>\n                <animate attributeName=\"y\" attributeType=\"XML\" begin=\"0s; anim4.end\" dur=\"0.75s\" values=\"20;16.5;16\" fill=\"freeze\"/>\n                <animate id=\"anim4\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim3.end\" dur=\"0.5s\" values=\"1;0;0\" fill=\"freeze\"/>\n              </rect>\n              <rect class=\"svg-rectangle\" id=\"Rectangle-2\" rx=\"1\" ry=\"1\" x=\"60\" y=\"16\" width=\"21\" height=\"21\" opacity=\"0\">\n                <animate id=\"anim2\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim1.end\" dur=\"0.75s\" values=\"0;1\" fill=\"freeze\"/>\n                <animate attributeName=\"y\" attributeType=\"XML\" begin=\"anim1.end\" dur=\"0.75s\" values=\"20;16.5;16\" fill=\"freeze\"/>\n                <animate id=\"anim5\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim3.end + 63ms\" dur=\"0.5s\" values=\"1;0;0\" fill=\"freeze\"/>\n              </rect>\n              <rect class=\"svg-rectangle\" id=\"Rectangle-3\" rx=\"1\" ry=\"1\" x=\"97\" y=\"16\" width=\"21\" height=\"21\" opacity=\"0\">\n                <animate id=\"anim3\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim2.end\" dur=\"0.75s\" values=\"0;1\" fill=\"freeze\"/>\n                <animate attributeName=\"y\" attributeType=\"XML\" begin=\"anim2.end\" dur=\"0.75s\" values=\"20;16.5;16\" fill=\"freeze\"/>\n                <animate id=\"anim6\" attributeName=\"opacity\" attributeType=\"XML\" begin=\"anim3.end + 125ms\" dur=\"0.5s\" values=\"1;0;0\" fill=\"freeze\"/>\n              </rect>\n            </g>\n          </g>\n        </svg>\n      </div>";
            }
        }]);

        return loadMoreAlbumsLanding;
    })();

    module.exports = loadMoreAlbumsLanding;
});