define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   * Passes values from one or multiple inputs to another after interaction
   *
   * @since 0.2.02
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2016 State Government of NSW
   *
   * @class
   * @requires jQuery
   */

  var InputValueTransferrer = (function () {
    function InputValueTransferrer(selector, config) {
      _classCallCheck(this, InputValueTransferrer);

      /**
       *
       * @param {string} config.selector: the element which has a value to transfer
       *
       */
      this.config = {
        selector: "[data-gef-input-transferrer]"
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Check if selector has been passed to constructor
      if (selector) {
        // Use jQuery to match find the relevant DOM element(s)
        this.$inputs = (0, _jquery)(selector);
        // Set the receiver element
        this.$receiver = (0, _jquery)("#" + this.$inputs.eq(0).attr(this.config.selector.replace(/\[|\]/g, "")));

        this.setChangeEvents(this.$inputs, this.$receiver);
      }
    }

    /**
     * Sets the events needed to capture input changes
     *
     * @setChangeEvents
     *
     * @param {jQuery} $i - A jQuery object with relevant input elements
     * @param {jQuery} $r - A jQuery object with relevant receiver element
     *
     */

    _createClass(InputValueTransferrer, [{
      key: "setChangeEvents",
      value: function setChangeEvents($i, $r) {
        var _this = this;

        $i.on("change", function () {
          _this.setReceiverValue($r, (0, _jquery)(this).val());
        });
      }

      /**
       * Sets the receiver values based on the changed input
       *
       * @setReceiverValue
       *
       * @param {jQuery} $r - A jQuery object with relevant receiver element
       * @param {String} v - The value to set on the receiver
       *
       */
    }, {
      key: "setReceiverValue",
      value: function setReceiverValue($r, v) {
        $r.val(v);
      }
    }]);

    return InputValueTransferrer;
  })();

  module.exports = InputValueTransferrer;
});