define(["exports", "jquery"], function (exports, _jquery) {
    "use strict";

    /**
     *
     * @since 1.0.0
     *
     * @author Digital Services <communications@det.nsw.edu.au>
     * @copyright © 2015 State Government of NSW 2015
     *
     * @class
     * @requires jQuery
     */

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
        var results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    // add css according to querystring

    function setTheme() {
        var theme = getParameterByName('theme');
        var themeType = getParameterByName('themeType');
        console.log("found theme", theme);
        if (theme) {
            var link = document.createElement('link');
            if (themeType) {
                link.href = "../assets/css/" + theme + "-" + themeType + ".css";
            } else {
                link.href = "../assets/css/" + theme + ".css";
            }
            link.rel = 'stylesheet';
            document.getElementsByTagName("head")[0].appendChild(link);
            // also set the theme name as a class of the body
            document.getElementsByTagName("body")[0].classList.add(theme);
        }
    }

    setTheme();
});