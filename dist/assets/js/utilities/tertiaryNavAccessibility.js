define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var ariaToggleMenu = (function () {

    /**
     * Script for Google translate feature
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     *
     */

    function ariaToggleMenu(selector, config) {
      _classCallCheck(this, ariaToggleMenu);

      console.log('tertiary nav constructor', selector);

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Observer pattern for changing aria-expanded attribute from li to child element a
      var attrObserver = this.initMutationObserver();
      // Constructor Variables
      this.el = (0, _jquery)(selector);
      this.ariaExpandedState;
      // this.expandButton = this.el.find('.sws-side__open-icon')
      this.sublistParent = selector.querySelectorAll('.sws-side__sublist-block');

      for (var i = 0; i < this.sublistParent.length; i++) {
        attrObserver.observe(this.sublistParent[i], { attributes: true });
      }

      console.log('tertiary nav constructor End');
    }

    _createClass(ariaToggleMenu, [{
      key: "initMutationObserver",
      value: function initMutationObserver() {
        var _this = this;

        return new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName == 'aria-expanded') {
              var target = (0, _jquery)(mutation.target)[0];
              _this.attributeChangeHandler(target);
            }
          });
        });
      }
    }, {
      key: "attributeChangeHandler",
      value: function attributeChangeHandler(element) {
        // Called everytime the aria-expanded attribute of li.uk-parent changes
        var el = (0, _jquery)(element);
        var state = el.attr('aria-expanded');
        // make sure the event is valid by checking of the attribute exists
        if (typeof state !== 'undefined') {
          // remove the attribute if it exists
          el.removeAttr('aria-expanded');
          var parentTitle = el.find('.sws-side__sublist-item .sws-side__list-item span').first().text().toString();
          var expandButton = el.children('.sws-side__open-icon');
          var currentState = expandButton.attr('aria-expanded');
          var labelString = state === 'true' ? ' - sub menu open' : ' - sub menu closed';
          labelString = parentTitle + labelString;

          // Add the attribute
          expandButton.attr('aria-expanded', state);
          expandButton.css("max-height", expandButton.prev()[0].clientHeight - 7 + 'px');
          expandButton.attr('aria-label', labelString);
        }
      }
    }]);

    return ariaToggleMenu;
  })();

  module.exports = ariaToggleMenu;
});