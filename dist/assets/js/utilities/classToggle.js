define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
   * A class toggling script designed for view changes only. Do not use for showing and hiding content.
   * Instead use AriaToggler and apply CSS on the attribute changes.
   *
   * @since 1.0.0
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   * @requires jQuery
   */

  var ClassToggle = (function () {

    /**
     * Creates a new Class Toggle
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options. Options vary depending on need
     * @param {String} config.selectors.active - class selector for the active state of toggle buttons
     *
     */

    function ClassToggle(selector, config) {
      _classCallCheck(this, ClassToggle);

      // Default class config options
      this.config = {
        selectors: {
          active: "uk-active"
        }
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
        console.log(this.config, config);
      }

      // Check if selector has been passed to constructor
      if (selector) {
        // Use jQuery to match find the relevant DOM element(s)
        this.$selector = (0, _jquery)(selector);
        // Class to toggle
        this.toggleClass = this.$selector.data("gef-class-toggle");
        // Element to toggle class on
        this.toggleTarget = this.$selector.data("gef-class-toggle-target");
        // Initiate event listener for click of button
        this.$selector.on("click", this.clickLogic.bind(this));
      }
    }

    /**
    * Exports the Class Toggle class as a module
    * @module
    */

    _createClass(ClassToggle, [{
      key: "clickLogic",
      value: function clickLogic() {
        // If disable is on don't allow event if active
        if (this.$selector.data("gef-class-toggle-disable")) {
          // If the current button isn't active
          if (!this.$selector.hasClass(this.config.selectors.active)) {
            // Remove active class on all buttons for target
            (0, _jquery)("[data-gef-class-toggle-target='" + this.toggleTarget + "']").removeClass(this.config.selectors.active);
            // Add active class to clicked button
            this.$selector.addClass(this.config.selectors.active);
            // Toggle class of target
            (0, _jquery)(this.toggleTarget).toggleClass(this.toggleClass);
          }
        } else {
          // If target has toggle class
          if (this.toggleTarget.hasClass(this.toggleClass)) {
            // Remove active class from button
            this.$selector.removeClass(this.config.selectors.active);
            // Remove toggle class from target
            (0, _jquery)(this.toggleTarget).removeClass(this.toggleClass);
          } else {
            // Add active class to button
            this.$selector.addClass(this.config.selectors.active);
            // Add toggle class to target
            (0, _jquery)(this.toggleTarget).addClass(this.toggleClass);
          }
        }
        return this.$selector;
      }
    }]);

    return ClassToggle;
  })();

  module.exports = ClassToggle;
});