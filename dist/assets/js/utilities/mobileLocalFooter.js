define(["exports", "module", "jquery"], function (exports, module, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  /**
  * mobileLocalFooter.js - utility which displays accordian for mobile view
  *
  * @since 1.1.12
  *
  * @author Digital Services <communications@det.nsw.edu.au>
  * @copyright © 2016 State Government of NSW 2016
  *
  * @class
  * @requires jQuery
  */

  var mobileLocalFooter = (function () {

    /**
     *
     * @param {jquery} selector - the element which is modified
     * @param {string} config.media_query - the media query which when triggers forces linking
     *
     */

    function mobileLocalFooter(selector, config) {
      _classCallCheck(this, mobileLocalFooter);

      this.selector = selector;
      console.log("1", this.selector);

      this.config = {
        media_query: "(min-width: 960px)"
      };

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // set the mediq query
      var media_query = window.matchMedia(this.config.media_query);
      // Test the media query
      this.testMediaQuery(media_query, selector);
    }

    /**
     * Tests the media query
     *
     * @param {string} mq - the media query string
     */

    _createClass(mobileLocalFooter, [{
      key: "testMediaQuery",
      value: function testMediaQuery(mq, selector) {
        if (mq.matches) {
          this.changeFooter(selector);
        }
      }

      /**
       * Forces linking on click
       *
       * @param {jquery} selector - the element in question
       */
    }, {
      key: "changeFooter",
      value: function changeFooter(selector) {
        (0, _jquery)(selector).addClass('gef-show-hide gef-show-hide--alt uk-accordion uk-visible-small');
        (0, _jquery)(selector).closest('h3').addClass('uk-accordion-title');
        (0, _jquery)(selector).closest('div').addClass('uk-accordion-content');
        console.log("9", this.selector);
      }
    }]);

    return mobileLocalFooter;
  })();

  module.exports = mobileLocalFooter;
});