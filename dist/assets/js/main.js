define(["exports"], function (exports) {
  "use strict";

  (function (doc) {
    "use strict";

    // Ensures there will be no 'console is undefined' errors
    window.console = window.console || (function () {
      var c = {};
      c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () {};
      return c;
    })();

    // Get the git repo revision hash specified in the <head> tag
    var gitRevision = doc.head || doc.getElementsByTagName("head")[0];
    gitRevision = gitRevision.getAttribute("data-revision");

    // Get the theme specified in the <head> tag
    var theme = doc.head || doc.getElementsByTagName("head")[0];
    theme = theme.getAttribute("data-theme");

    // Get the path to the site level config
    var siteConfigPath = doc.querySelectorAll("[data-site-config-path]")[0].getAttribute("data-site-config-path");

    // RequireJS config
    var requireConfig = {
      paths: {
        config: "config/config",
        siteConfig: "" + siteConfigPath + "siteConfig",
        vendor: "vendor",
        core: "core",
        utilities: "utilities",
        jquery: "vendor/jquery",
        keycodes: "core/keyCodes",
        spin: "vendor/spin/spin",
        uikit: "vendor/uikit/uikit",
        placeholder: "vendor/jquery-placeholder",
        angular: "vendor/angular/angular",
        angularSanitize: "vendor/angular-sanitize/angular-sanitize",
        angularAria: "vendor/angular-aria/angular-aria",
        angularUtilsPaginate: "vendor/angular-utils-pagination/dirPagination",
        jqueryAccessibleTabs: "vendor/jquery-accessible-tabpanel-aria/jquery-accessible-tabs",
        fancybox: "vendor/fancybox/jquery.fancybox.min",
        justifiedGallery: "vendor/justifiedGallery/jquery.justifiedGallery.min"
      },
      config: {
        uikit: {
          base: "vendor/uikit"
        }
      },
      map: {
        "*": {
          css: "vendor/requirejs-plugins/css/css"
        }
      },
      shim: {
        uikit: ["jquery"],
        placeholder: ["jquery"],
        annyang: {
          exports: "annyang"
        },
        angular: {
          exports: "angular"
        },
        angularSanitize: ["angular"],
        angularUtilsPaginate: ["angular"],
        searchSPA: ["jquery"],
        datePicker: ["uikit"],
        jqueryAccessibleTabs: ["jquery"],
        fancybox: ["jquery"],
        justifiedGallery: ["jquery"]
      },
      packages: ["components/socialMedia"],
      bundles: {
        "apps/search/searchSPA": ["searchSPA", "angular", "angularSanitize", "angularUtilsPaginate"]
      }
    };

    // Set RequireJS config options
    require.config(requireConfig);

    // array of scripts to be loaded
    var requireArr = ["core/componentLoader", "jquery", "config"];

    // add the site config if it exists
    if (siteConfigPath) {
      requireArr.push("siteConfig");
    }

    // Main initaliser. Loads required javascripts based on what components are
    // present on the page and what components have been registered in the themes
    // config
    require(requireArr, function (ComponentLoader, $, config, siteConfig) {

      // Put jQuery into noConflict mode
      $.noConflict();

      // Make sure the theme config has loaded before trying to load it
      if (config) {
        var loader = new ComponentLoader(config);
        loader;
      } else {
        console.log("Error loading theme config");
      }

      //Make sure the site config has loaded before trying to load it
      if (siteConfig) {
        var loader = new ComponentLoader(siteConfig);
        loader;
      }
    });
  })(document);
});