define(["exports", "module", "reactRedux", "apps/health-safety/components/Providers/presenter", "reactPureLifecycle", "apps/health-safety/actions/providers", "apps/health-safety/actions/selectedproviders"], function (exports, module, _reactRedux, _appsHealthSafetyComponentsProvidersPresenter, _reactPureLifecycle, _appsHealthSafetyActionsProviders, _appsHealthSafetyActionsSelectedproviders) {
    //import React from "react";
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _Providers = _interopRequireDefault(_appsHealthSafetyComponentsProvidersPresenter);

    var _lifecycle = _interopRequireDefault(_reactPureLifecycle);

    function mapStateToProps(state) {
        var providers = state.providers;
        var selectedschool = state.selectedschool;
        var networkcode = state.networkcode;
        var selectedproviders = state.selectedproviders;
        return {
            providers: providers,
            selectedschool: selectedschool,
            networkcode: networkcode,
            selectedproviders: selectedproviders
        };
    }

    function mapDispatchToProps(dispatch) {
        return {
            onGetProviders: function onGetProviders() {
                return dispatch((0, _appsHealthSafetyActionsProviders.getProviders)());
            },
            onGetSelectedProviders: function onGetSelectedProviders(networkcode, providers) {
                return (0, _appsHealthSafetyActionsSelectedproviders.setSelectedProviders)(dispatch, networkcode, providers);
            }
        };
    }

    var componentWillMount = function componentWillMount(store, networkcode) {
        store.onGetProviders();
        store.onGetSelectedProviders(networkcode, { data: { id: -1 } });
    };

    var cwm = (0, _lifecycle["default"])({
        componentWillMount: componentWillMount
    })(_Providers["default"]);

    module.exports = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(cwm);
});