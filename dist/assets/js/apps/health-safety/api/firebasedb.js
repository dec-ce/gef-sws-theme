define(["exports", "module", "firebase"], function (exports, module, _firebase) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _firebase2 = _interopRequireDefault(_firebase);

    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCgaHcrd8CrMH9C1Aa8MejxGKLqXe0BMSw",
        authDomain: "doe-health-and-safety.firebaseapp.com",
        databaseURL: "https://doe-health-and-safety.firebaseio.com",
        projectId: "doe-health-and-safety",
        storageBucket: "doe-health-and-safety.appspot.com",
        messagingSenderId: "355216506926"
    };

    _firebase2["default"].initializeApp(config);
    module.exports = _firebase2["default"].database();
});