define(["exports", "module", "react", "apps/health-safety/components/SearchForm/index", "apps/health-safety/components/Providers/index"], function (exports, module, _react, _appsHealthSafetyComponentsSearchFormIndex, _appsHealthSafetyComponentsProvidersIndex) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var _SearchForm = _interopRequireDefault(_appsHealthSafetyComponentsSearchFormIndex);

    var _Providers = _interopRequireDefault(_appsHealthSafetyComponentsProvidersIndex);

    var App = function App() {
        return [_React["default"].createElement(_SearchForm["default"], { key: "1" }), _React["default"].createElement(_Providers["default"], { key: "2" })];
    };

    module.exports = App;
});