define(["exports", "module", "redux", "apps/health-safety/reducers/providers", "apps/health-safety/reducers/schools", "apps/health-safety/reducers/searchinput", "apps/health-safety/reducers/selectedschool", "apps/health-safety/reducers/invalidschool", "apps/health-safety/reducers/networkcode", "apps/health-safety/reducers/selectedproviders"], function (exports, module, _redux, _appsHealthSafetyReducersProviders, _appsHealthSafetyReducersSchools, _appsHealthSafetyReducersSearchinput, _appsHealthSafetyReducersSelectedschool, _appsHealthSafetyReducersInvalidschool, _appsHealthSafetyReducersNetworkcode, _appsHealthSafetyReducersSelectedproviders) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _providers = _interopRequireDefault(_appsHealthSafetyReducersProviders);

    var _schools = _interopRequireDefault(_appsHealthSafetyReducersSchools);

    var _searchinput = _interopRequireDefault(_appsHealthSafetyReducersSearchinput);

    var _selectedschool = _interopRequireDefault(_appsHealthSafetyReducersSelectedschool);

    var _invalidschool = _interopRequireDefault(_appsHealthSafetyReducersInvalidschool);

    var _networkcode = _interopRequireDefault(_appsHealthSafetyReducersNetworkcode);

    var _selectedproviders = _interopRequireDefault(_appsHealthSafetyReducersSelectedproviders);

    module.exports = (0, _redux.combineReducers)({
        providers: _providers["default"],
        schools: _schools["default"],
        searchinput: _searchinput["default"],
        selectedschool: _selectedschool["default"],
        selectedproviders: _selectedproviders["default"],
        invalidschool: _invalidschool["default"],
        networkcode: _networkcode["default"]
    });
});