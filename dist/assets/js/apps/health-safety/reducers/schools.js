define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.SCHOOLS_GET_FULFILLED:
                return setSchools(state, action);
            default:
                return state;
        }
    };

    function setSchools(state, action) {
        var schools = action.schools;

        return schools;
    }
});