define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.SELECTEDSCHOOL_SET:
                return setInvalidSchool(state, action);
            default:
                return state;
        }
    };

    function setInvalidSchool(state, action) {
        var selectedschool = action.selectedschool;

        if (!selectedschool) {
            return "Please enter a valid school name";
        } else {
            return null;
        }
    }
});