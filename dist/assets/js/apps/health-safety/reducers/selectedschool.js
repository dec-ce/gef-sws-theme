define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.SELECTEDSCHOOL_SET:
                return setSelectedSchool(state, action);
            default:
                return state;
        }
    };

    function setSelectedSchool(state, action) {
        var selectedschool = action.selectedschool;

        if (!selectedschool) {
            return null;
        } else {
            return selectedschool;
        }
    }
});