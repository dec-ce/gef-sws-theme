define(["exports", "module", "apps/health-safety/constants/actionTypes"], function (exports, module, _appsHealthSafetyConstantsActionTypes) {
    "use strict";

    var initialState = [];

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _appsHealthSafetyConstantsActionTypes.NETWORK_CODE_GET_FULFILLED:
                return setNetworkCode(state, action);
            default:
                return state;
        }
    };

    function setNetworkCode(state, action) {
        var networkcode = action.networkcode;

        if (networkcode === null) {
            return state;
        } else {
            return networkcode;
        }
    }
});