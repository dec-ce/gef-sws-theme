define(["exports", "apps/health-safety/constants/actionTypes", "lodash", "apps/health-safety/actions/selectedproviders", "axios"], function (exports, _appsHealthSafetyConstantsActionTypes, _lodash, _appsHealthSafetyActionsSelectedproviders, _axios) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _axios2 = _interopRequireDefault(_axios);

    var setSelectedSchool = (0, _lodash.curry)(function (dispatch, selectedschool, providers) {
        dispatch({
            type: _appsHealthSafetyConstantsActionTypes.SELECTEDSCHOOL_SET,
            selectedschool: selectedschool
        });

        if (!selectedschool) return;
        setNetworkCode(dispatch, selectedschool, providers);
    });

    exports.setSelectedSchool = setSelectedSchool;
    var setNetworkCode = function setNetworkCode(dispatch, selectedschool, providers) {
        // Now also get the network code of the school
        dispatch(getNetworkCodeRequestedAction());

        return _axios2["default"].get("https://doe-health-and-safety.firebaseio.com/networks.json?orderBy=\"name\"&equalTo=\"" + selectedschool.network + "\"").then(function (response) {
            var network = response.data;
            if (network === null) {
                dispatch(getNetworkCodeRejectedAction());
                return;
            }

            var networkcode = Object.keys(network)[0];
            dispatch(getNetworkCodeFulfilledAction(networkcode));

            (0, _appsHealthSafetyActionsSelectedproviders.setSelectedProviders)(dispatch, networkcode, providers);
        })["catch"](function (error) {
            console.log(error);
            dispatch(getNetworkCodeRejectedAction());
        });
    };

    exports.setNetworkCode = setNetworkCode;
    function getNetworkCodeRequestedAction() {
        return {
            type: _appsHealthSafetyConstantsActionTypes.NETWORK_CODE_GET_REQUESTED
        };
    }

    function getNetworkCodeFulfilledAction(networkcode) {
        return {
            type: _appsHealthSafetyConstantsActionTypes.NETWORK_CODE_GET_FULFILLED,
            networkcode: networkcode
        };
    }

    function getNetworkCodeRejectedAction() {
        return {
            type: _appsHealthSafetyConstantsActionTypes.NETWORK_CODE_GET_REJECTED
        };
    }
});