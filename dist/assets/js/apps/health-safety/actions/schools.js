define(["exports", "apps/health-safety/constants/actionTypes", "axios"], function (exports, _appsHealthSafetyConstantsActionTypes, _axios) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getSchools = getSchools;

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _axios2 = _interopRequireDefault(_axios);

    function getSchools() {
        return function (dispatch) {
            dispatch(getSchoolsRequestedAction());

            return _axios2["default"].get("https://doe-health-and-safety.firebaseio.com/schools.json").then(function (response) {
                var schools = response.data;
                dispatch(getSchoolsFulfilledAction(schools));
            })["catch"](function (error) {
                console.log(error);
                dispatch(getSchoolsRejectedAction());
            });
        };
    }

    function getSchoolsRequestedAction() {
        return {
            type: _appsHealthSafetyConstantsActionTypes.SCHOOLS_GET_REQUESTED
        };
    }

    function getSchoolsFulfilledAction(schools) {
        return {
            type: _appsHealthSafetyConstantsActionTypes.SCHOOLS_GET_FULFILLED,
            schools: schools
        };
    }

    function getSchoolsRejectedAction() {
        return {
            type: _appsHealthSafetyConstantsActionTypes.SCHOOLS_GET_REJECTED
        };
    }
});