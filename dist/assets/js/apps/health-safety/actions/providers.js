define(["exports", "apps/health-safety/constants/actionTypes", "axios"], function (exports, _appsHealthSafetyConstantsActionTypes, _axios) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getProviders = getProviders;

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _axios2 = _interopRequireDefault(_axios);

    function getProviders() {
        return function (dispatch) {
            dispatch(getProvidersRequestedAction());

            return _axios2["default"].get("https://doe-health-and-safety.firebaseio.com/providers.json").then(function (response) {
                var providers = response.data;
                dispatch(getProvidersFulfilledAction(providers));
            })["catch"](function (error) {
                console.log(error);
                dispatch(getProvidersRejectedAction());
            });
        };
    }

    function getProvidersRequestedAction() {
        return {
            type: _appsHealthSafetyConstantsActionTypes.PROVIDERS_GET_REQUESTED
        };
    }

    function getProvidersFulfilledAction(providers) {
        return {
            type: _appsHealthSafetyConstantsActionTypes.PROVIDERS_GET_FULFILLED,
            providers: providers
        };
    }

    function getProvidersRejectedAction() {
        return {
            type: _appsHealthSafetyConstantsActionTypes.PROVIDERS_GET_REJECTED
        };
    }
});