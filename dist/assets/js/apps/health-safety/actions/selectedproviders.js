define(["exports", "apps/health-safety/constants/actionTypes", "lodash", "axios"], function (exports, _appsHealthSafetyConstantsActionTypes, _lodash, _axios) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.setSelectedProviders = setSelectedProviders;

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _2 = _interopRequireDefault(_lodash);

    var _axios2 = _interopRequireDefault(_axios);

    function setSelectedProviders(dispatch, networkcode, providers) {
        dispatch({
            type: _appsHealthSafetyConstantsActionTypes.SELECTEDPROVIDERS_CLEAR
        });

        _2["default"].transform(providers, function (result, value, key) {
            var trainers = 0;
            var ncode = "";
            _2["default"].forEach(value.networks, function (network) {
                if (network.code === networkcode) {
                    trainers = network.trainers;
                    ncode = network.code;
                }
            });

            if (trainers !== 0) {
                var _ret = (function () {
                    var combinedmin = 0,
                        combinedmax = 0,
                        anamin = 0,
                        anamax = 0,
                        cprmin = 0,
                        cprmax = 0;

                    return {
                        v: _axios2["default"].get("https://doe-health-and-safety.firebaseio.com/courses.json?orderBy=\"provider\"&equalTo=\"" + key + "\"").then(function (response) {
                            _2["default"].forEach(response.data, function (course) {
                                if ("CPR" === course.name) {
                                    cprmin = course.cost.min;
                                    cprmax = course.cost.max;
                                } else if ("Anaphylaxis" === course.name) {
                                    anamin = course.cost.min;
                                    anamax = course.cost.max;
                                } else if ("Anaphylaxis/CPR" === course.name) {
                                    combinedmin = course.cost.min;
                                    combinedmax = course.cost.max;
                                }
                            });

                            var combined = "";
                            if (!combinedmin) combined = "$" + combinedmax + "*";
                            if (!combinedmax) combined = "$" + combinedmin + "*";
                            if (combinedmin && combinedmax) combined = "$" + combinedmin + " - $" + combinedmax + "*";

                            var anaphylaxis = "";
                            if (!anamin) anaphylaxis = "$" + anamax + "*";
                            if (!anamax) anaphylaxis = "$" + anamin + "*";
                            if (anamin && anamax) anaphylaxis = "$" + anamin + " - $" + anamax + "*";

                            var cpr = "";
                            if (!cprmin) cpr = "$" + cprmax + "*";
                            if (!cprmax) cpr = "$" + cprmin + "*";
                            if (cprmin && cprmax) cpr = "$" + cprmin + " - $" + cprmax + "*";

                            var selectedprovider = {
                                name: value.name,
                                email: value.email,
                                contact: value.contact,
                                phone: value.phone,
                                network: ncode,
                                trainers: trainers,
                                combined: combined,
                                anaphylaxis: anaphylaxis,
                                cpr: cpr
                            };

                            dispatch({
                                type: _appsHealthSafetyConstantsActionTypes.SELECTEDPROVIDERS_ADD,
                                selectedprovider: selectedprovider
                            });
                        })["catch"](function (error) {
                            console.log(error);
                        })
                    };
                })();

                if (typeof _ret === "object") return _ret.v;
            }
        });
    }
});