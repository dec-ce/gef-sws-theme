define(["exports", "module", "redux", "reduxLogger", "reduxThunk", "apps/health-safety/reducers/index"], function (exports, module, _redux, _reduxLogger, _reduxThunk, _appsHealthSafetyReducersIndex) {
    "use strict";

    module.exports = configureStore;

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _thunk = _interopRequireDefault(_reduxThunk);

    var _rootReducer = _interopRequireDefault(_appsHealthSafetyReducersIndex);

    var logger = (0, _reduxLogger.createLogger)();
    var createStoreWithMiddleware = (0, _redux.applyMiddleware)(_thunk["default"], logger)(_redux.createStore);

    function configureStore(initialState) {
        return createStoreWithMiddleware(_rootReducer["default"], initialState);
    }
});