define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.REGIONS_SET:
                return setRegions(state, action);
            default:
                return state;
        }
    };

    function setRegions(state, action) {
        var regions = action.regions;

        return regions;
    }
});