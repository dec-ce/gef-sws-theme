define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.SELECTEDREGION_SET:
                return setSelectedRegion(state, action);
            default:
                return state;
        }
    };

    function setSelectedRegion(state, action) {
        var selectedregion = action.selectedregion;

        return selectedregion;
    }
});