define(["exports", "module", "redux", "./schools", "./searchinput", "./bushfireprone", "./selectedschools", "./regions", "./selectedregion", "./regionschools"], function (exports, module, _redux, _schools, _searchinput, _bushfireprone, _selectedschools, _regions, _selectedregion, _regionschools) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _schools2 = _interopRequireDefault(_schools);

    var _searchinput2 = _interopRequireDefault(_searchinput);

    var _bushfireprone2 = _interopRequireDefault(_bushfireprone);

    var _selectedschools2 = _interopRequireDefault(_selectedschools);

    var _regions2 = _interopRequireDefault(_regions);

    var _selectedregion2 = _interopRequireDefault(_selectedregion);

    var _regionschools2 = _interopRequireDefault(_regionschools);

    module.exports = (0, _redux.combineReducers)({
        regions: _regions2["default"],
        regionschools: _regionschools2["default"],
        schools: _schools2["default"],
        searchinput: _searchinput2["default"],
        bushfireprone: _bushfireprone2["default"],
        selectedschools: _selectedschools2["default"],
        selectedregion: _selectedregion2["default"]
    });
});