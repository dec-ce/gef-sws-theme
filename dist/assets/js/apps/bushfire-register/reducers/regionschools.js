define(["exports", "module", "../constants/actionTypes"], function (exports, module, _constantsActionTypes) {
    "use strict";

    var initialState = null;

    module.exports = function (state, action) {
        if (state === undefined) state = initialState;

        switch (action.type) {
            case _constantsActionTypes.REGIONSCHOOLS_SET:
                return setRegionSchools(state, action);
            default:
                return state;
        }
    };

    function setRegionSchools(state, action) {
        var regionschools = action.regionschools;

        return regionschools;
    }
});