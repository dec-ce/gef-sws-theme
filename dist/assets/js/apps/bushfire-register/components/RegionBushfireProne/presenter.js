define(["exports", "module", "react"], function (exports, module, _react) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var RegionBushfireProne = function RegionBushfireProne(props) {
        var regionschools = props.regionschools;

        if (regionschools) {
            return _React["default"].createElement(
                "div",
                { id: "resultsRegion", style: { display: "block" } },
                _React["default"].createElement(
                    "table",
                    null,
                    _React["default"].createElement(
                        "thead",
                        null,
                        _React["default"].createElement(
                            "tr",
                            null,
                            _React["default"].createElement(
                                "th",
                                null,
                                "School"
                            ),
                            _React["default"].createElement(
                                "th",
                                null,
                                "NSW fire area"
                            ),
                            _React["default"].createElement(
                                "th",
                                null,
                                "Busfire prone"
                            ),
                            _React["default"].createElement(
                                "th",
                                null,
                                "High risk"
                            )
                        )
                    ),
                    _React["default"].createElement(
                        "tbody",
                        null,
                        regionschools.map(function (school, key) {

                            return _React["default"].createElement(
                                "tr",
                                { key: key },
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    school.name
                                ),
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    school.nswFireArea
                                ),
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    school.bushfireProne ? 'Yes' : 'No'
                                ),
                                _React["default"].createElement(
                                    "td",
                                    null,
                                    school.highRisk ? 'Yes' : 'No'
                                )
                            );
                        })
                    )
                )
            );
        } else {
            return "";
        }
    };

    module.exports = RegionBushfireProne;
});