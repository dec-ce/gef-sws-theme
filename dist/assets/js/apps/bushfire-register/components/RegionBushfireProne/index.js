define(["exports", "module", "reactRedux", "reactPureLifecycle", "apps/bushfire-register/components/RegionBushfireProne/presenter"], function (exports, module, _reactRedux, _reactPureLifecycle, _appsBushfireRegisterComponentsRegionBushfirePronePresenter) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _lifecycle = _interopRequireDefault(_reactPureLifecycle);

    var _RegionBushfireProne = _interopRequireDefault(_appsBushfireRegisterComponentsRegionBushfirePronePresenter);

    function mapStateToProps(state) {
        var regionschools = state.regionschools;
        return {
            regionschools: regionschools
        };
    }

    var componentWillMount = function componentWillMount(store) {};

    var cwm = (0, _lifecycle["default"])({
        componentWillMount: componentWillMount
    })(_RegionBushfireProne["default"]);

    module.exports = (0, _reactRedux.connect)(mapStateToProps)(cwm);
});