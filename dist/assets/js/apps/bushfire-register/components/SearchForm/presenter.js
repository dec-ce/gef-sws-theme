define(["exports", "module", "react", "reactAutocomplete"], function (exports, module, _react, _reactAutocomplete) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var _ReactAutocomplete = _interopRequireDefault(_reactAutocomplete);

    var SearchForm = function SearchForm(props) {
        var schools = props.schools;
        var searchinput = props.searchinput;
        var bushfireprone = props.bushfireprone;
        var onSetSearchInput = props.onSetSearchInput;
        var onSetSelectedSchools = props.onSetSelectedSchools;

        var handleSearch = function handleSearch(event) {
            event.preventDefault();
            onSetSelectedSchools(searchinput, bushfireprone);
        };

        if (!schools) {
            return "";
        } else {
            return _React["default"].createElement(
                "div",
                { className: "bushfire-search" },
                _React["default"].createElement(
                    "form",
                    { name: "frmSchool", id: "frmSchool", className: "gef-search uk-form gef-search--body", onSubmit: handleSearch },
                    _React["default"].createElement(
                        "fieldset",
                        null,
                        _React["default"].createElement(
                            "legend",
                            { className: "show-on-sr" },
                            "Search the departments bushfire register"
                        ),
                        _React["default"].createElement(
                            "label",
                            { htmlFor: "school_search_input" },
                            _React["default"].createElement(
                                "span",
                                { className: "show-on-sr" },
                                "Search the departments bushfire register"
                            )
                        ),
                        _React["default"].createElement(_ReactAutocomplete["default"], {
                            inputProps: {
                                id: "school_search_input",
                                size: 50,
                                "aria-haspopup": true,
                                maxLength: 254,
                                type: "text",
                                name: "schoolName",
                                className: "ui-autocomplete-input"
                            },
                            items: schools,
                            shouldItemRender: function (item, value) {
                                return value.length >= 3 && item.toLowerCase().indexOf(value.toLowerCase()) > -1;
                            },
                            getItemValue: function (item) {
                                return item;
                            },
                            renderItem: function (item, highlighted) {
                                return _React["default"].createElement(
                                    "div",
                                    { key: item,
                                        style: { backgroundColor: highlighted ? "#eee" : "transparent" } },
                                    item
                                );
                            },

                            value: searchinput === null ? "" : searchinput,
                            onChange: function (element) {
                                return onSetSearchInput(element.target.value);
                            },
                            onSelect: function (value) {
                                return onSetSelectedSchools(value, bushfireprone);
                            }
                        }),
                        _React["default"].createElement(
                            "button",
                            { type: "button", id: "submitSchoolSearch", "aria-label": "Submit search", tabIndex: "0", onClick: handleSearch },
                            "SEARCH"
                        )
                    )
                )
            );
        }
    };

    module.exports = SearchForm;
});