define(["exports", "module", "reactRedux", "reactPureLifecycle", "apps/bushfire-register/components/SearchForm/presenter", "apps/bushfire-register/actions/searchinput", "apps/bushfire-register/actions/selectedschools", "apps/bushfire-register/actions/bushfireprone"], function (exports, module, _reactRedux, _reactPureLifecycle, _appsBushfireRegisterComponentsSearchFormPresenter, _appsBushfireRegisterActionsSearchinput, _appsBushfireRegisterActionsSelectedschools, _appsBushfireRegisterActionsBushfireprone) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _lifecycle = _interopRequireDefault(_reactPureLifecycle);

    var _SearchForm = _interopRequireDefault(_appsBushfireRegisterComponentsSearchFormPresenter);

    function mapStateToProps(state) {
        var schools = state.schools;
        var searchinput = state.searchinput;
        var bushfireprone = state.bushfireprone;
        return {
            schools: schools,
            searchinput: searchinput,
            bushfireprone: bushfireprone
        };
    }

    function mapDispatchToProps(dispatch) {
        return {
            onGetBushfireProne: function onGetBushfireProne() {
                return dispatch((0, _appsBushfireRegisterActionsBushfireprone.getBushfireProne)());
            },
            onSetSearchInput: function onSetSearchInput(searchinput) {
                return dispatch((0, _appsBushfireRegisterActionsSearchinput.setSearchInput)(searchinput));
            },
            onSetSelectedSchools: function onSetSelectedSchools(searchinput, bushfireprone) {
                dispatch((0, _appsBushfireRegisterActionsSearchinput.setSearchInput)(searchinput));
                dispatch((0, _appsBushfireRegisterActionsSelectedschools.setSelectedSchools)(searchinput, bushfireprone));
            }
        };
    }

    var componentWillMount = function componentWillMount(store) {
        store.onGetBushfireProne();
        store.onSetSearchInput("");
    };

    var cwm = (0, _lifecycle["default"])({
        componentWillMount: componentWillMount
    })(_SearchForm["default"]);

    module.exports = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(cwm);
});