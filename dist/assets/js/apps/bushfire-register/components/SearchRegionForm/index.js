define(["exports", "module", "reactRedux", "reactPureLifecycle", "apps/bushfire-register/components/SearchRegionForm/presenter", "apps/bushfire-register/actions/selectedregion", "apps/bushfire-register/actions/bushfireproneregion"], function (exports, module, _reactRedux, _reactPureLifecycle, _appsBushfireRegisterComponentsSearchRegionFormPresenter, _appsBushfireRegisterActionsSelectedregion, _appsBushfireRegisterActionsBushfireproneregion) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _lifecycle = _interopRequireDefault(_reactPureLifecycle);

    var _SearchForm = _interopRequireDefault(_appsBushfireRegisterComponentsSearchRegionFormPresenter);

    function mapStateToProps(state) {
        var regions = state.regions;
        var bushfireprone = state.bushfireprone;
        var selectedregion = state.selectedregion;
        return {
            regions: regions,
            bushfireprone: bushfireprone,
            selectedregion: selectedregion
        };
    }

    function mapDispatchToProps(dispatch) {
        return {
            onGetBushfireProneRegion: function onGetBushfireProneRegion(selectedregion, bushfireprone) {
                return dispatch((0, _appsBushfireRegisterActionsBushfireproneregion.getBushfireProneRegion)(selectedregion, bushfireprone));
            },
            onSetSelectedRegion: function onSetSelectedRegion(selectedregion) {
                return dispatch((0, _appsBushfireRegisterActionsSelectedregion.setSelectedRegion)(selectedregion));
            }
        };
    }

    var componentWillMount = function componentWillMount(store) {
        store.onSetSelectedRegion("default");
    };

    var cwm = (0, _lifecycle["default"])({
        componentWillMount: componentWillMount
    })(_SearchForm["default"]);

    module.exports = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(cwm);
});