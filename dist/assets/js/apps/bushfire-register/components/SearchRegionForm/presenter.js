define(["exports", "module", "react"], function (exports, module, _react) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var SearchRegionForm = function SearchRegionForm(props) {
        var regions = props.regions;
        var selectedregion = props.selectedregion;
        var bushfireprone = props.bushfireprone;
        var onGetBushfireProneRegion = props.onGetBushfireProneRegion;
        var onSetSelectedRegion = props.onSetSelectedRegion;

        var handleSelect = function handleSelect(event) {
            onSetSelectedRegion(event.target.value);
        };

        var handleClick = function handleClick(event) {
            event.preventDefault();
            onGetBushfireProneRegion(selectedregion, bushfireprone);
        };

        if (regions) {
            return _React["default"].createElement(
                "div",
                { className: "bushfire-search" },
                _React["default"].createElement(
                    "form",
                    { name: "frmRegion", id: "frmRegion",
                        className: "gef-form gef-form--inline uk-width-large-8-10 uk-margin-remove",
                        onSubmit: handleClick },
                    _React["default"].createElement(
                        "fieldset",
                        null,
                        _React["default"].createElement(
                            "legend",
                            { className: "show-on-sr" },
                            "Bush fire area list"
                        ),
                        _React["default"].createElement(
                            "label",
                            { id: "region_name_label", htmlFor: "region_name", className: "show-on-sr" },
                            "Search NSW fire area"
                        ),
                        _React["default"].createElement(
                            "select",
                            { name: "regionName", id: "region_name", value: selectedregion, onChange: handleSelect },
                            _React["default"].createElement(
                                "option",
                                { value: "default" },
                                "Please select a NSW fire area"
                            ),
                            regions.map(function (region, key) {
                                return _React["default"].createElement(
                                    "option",
                                    { key: key, value: region },
                                    region
                                );
                            })
                        ),
                        _React["default"].createElement(
                            "button",
                            { type: "button", id: "submit_school_firearea", "aria-label": "Submit search",
                                tabIndex: "0", onClick: handleClick },
                            "SEARCH"
                        )
                    )
                )
            );
        } else {
            return "";
        }
    };

    module.exports = SearchRegionForm;
});