define(["exports", "module", "react", "apps/bushfire-register/components/SearchForm/index", "apps/bushfire-register/components/BushfireProne/index", "apps/bushfire-register/components/SearchRegionForm/index", "apps/bushfire-register/components/RegionBushfireProne/index"], function (exports, module, _react, _appsBushfireRegisterComponentsSearchFormIndex, _appsBushfireRegisterComponentsBushfireProneIndex, _appsBushfireRegisterComponentsSearchRegionFormIndex, _appsBushfireRegisterComponentsRegionBushfireProneIndex) {
    "use strict";

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _React = _interopRequireDefault(_react);

    var _SearchForm = _interopRequireDefault(_appsBushfireRegisterComponentsSearchFormIndex);

    var _BushfireProne = _interopRequireDefault(_appsBushfireRegisterComponentsBushfireProneIndex);

    var _SearchRegionForm = _interopRequireDefault(_appsBushfireRegisterComponentsSearchRegionFormIndex);

    var _RegionBushfireProne = _interopRequireDefault(_appsBushfireRegisterComponentsRegionBushfireProneIndex);

    var App = function App() {
        return [_React["default"].createElement(
            "p",
            { key: "1" },
            "Please search for your school or site on the department\"s bush fire register below:"
        ), _React["default"].createElement(_SearchForm["default"], { key: "2" }), _React["default"].createElement(_BushfireProne["default"], { key: "3" }), _React["default"].createElement(
            "p",
            { key: "4" },
            _React["default"].createElement(
                "strong",
                null,
                "Search for a NSW public school or site by NSW fire area"
            )
        ), _React["default"].createElement(_SearchRegionForm["default"], { key: "5" }), _React["default"].createElement(_RegionBushfireProne["default"], { key: "6" })];
    };

    module.exports = App;
});