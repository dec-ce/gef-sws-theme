define(["exports", "lodash", "apps/bushfire-register/constants/actionTypes"], function (exports, _lodash, _appsBushfireRegisterConstantsActionTypes) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.getBushfireProneRegion = getBushfireProneRegion;

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

    var _2 = _interopRequireDefault(_lodash);

    function getBushfireProneRegion(selectedregion, bushfireprone) {
        return function (dispatch) {
            var unsortedregionschools = _2["default"].transform(bushfireprone, function (result, value) {
                if (value.nswFireArea === selectedregion && (value.bushfireProne || value.hishRisk)) {
                    result.push(value);
                    return true;
                }
            }, []);

            var regionschools = _2["default"].sortBy(unsortedregionschools, function (regionschool) {
                return regionschool.name;
            });
            dispatch(setRegionSchools(regionschools));
        };
    }

    function setRegionSchools(regionschools) {
        return {
            type: _appsBushfireRegisterConstantsActionTypes.REGIONSCHOOLS_SET,
            regionschools: regionschools
        };
    }
});