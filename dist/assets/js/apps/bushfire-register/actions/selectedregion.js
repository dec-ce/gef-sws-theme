define(["exports", "apps/bushfire-register/constants/actionTypes"], function (exports, _appsBushfireRegisterConstantsActionTypes) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var setSelectedRegion = function setSelectedRegion(selectedregion) {
        return function (dispatch) {
            dispatch({
                type: _appsBushfireRegisterConstantsActionTypes.SELECTEDREGION_SET,
                selectedregion: selectedregion
            });
        };
    };
    exports.setSelectedRegion = setSelectedRegion;
});