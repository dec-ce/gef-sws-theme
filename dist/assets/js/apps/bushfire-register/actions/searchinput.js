define(["exports", "apps/bushfire-register/constants/actionTypes"], function (exports, _appsBushfireRegisterConstantsActionTypes) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var setSearchInput = function setSearchInput(searchinput) {
        return function (dispatch) {
            dispatch({
                type: _appsBushfireRegisterConstantsActionTypes.SEARCHINPUT_SET,
                searchinput: searchinput
            });
        };
    };
    exports.setSearchInput = setSearchInput;
});