define(["exports", "module", "angular", "angularSanitize", "angularUtilsPaginate", "jquery", "apps/search/services/services", "apps/search/controllers/controllers", "apps/search/filters/filters", "apps/search/services/config", "apps/search/services/utils", "apps/search/controllers/searchFormCtrl", "apps/search/controllers/showResultsCtrl", "apps/search/filters/sanitize", "apps/search/services/search", "apps/search/aria/aria"], function (exports, module, _angular, _angularSanitize, _angularUtilsPaginate, _jquery, _appsSearchServicesServices, _appsSearchControllersControllers, _appsSearchFiltersFilters, _appsSearchServicesConfig, _appsSearchServicesUtils, _appsSearchControllersSearchFormCtrl, _appsSearchControllersShowResultsCtrl, _appsSearchFiltersSanitize, _appsSearchServicesSearch, _appsSearchAriaAria) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _angular2 = _interopRequireDefault(_angular);

  var _AngularSanitize = _interopRequireDefault(_angularSanitize);

  var _angularUtilsPaginate2 = _interopRequireDefault(_angularUtilsPaginate);

  var _services = _interopRequireDefault(_appsSearchServicesServices);

  var _controllers = _interopRequireDefault(_appsSearchControllersControllers);

  var _filters = _interopRequireDefault(_appsSearchFiltersFilters);

  /**
   * main.js
   * Initialising the search application using angular.js
   * Application is single page call to GSA resulting.
   * Third party angualar codes used are:
   *  - angular :  default angular code
   *  - angularSanitize : allows display of HTML code from jsonp responses
   *  - angularUtilsPaginate : handles pagination and standard display
   */

  var SearchSPA = (function () {
    function SearchSPA(selector, config) {
      _classCallCheck(this, SearchSPA);

      this.config = {};

      _jquery.extend(this.config, config);
      this.selector = (0, _jquery)(selector);

      // initiates the app on looking for the an item with ID SearchSPA.
      // specifies requires services, controllers and filters.
      this.app = _angular2["default"].module("SearchSPA", ["services", "controllers", "filters", "angularUtils.directives.dirPagination"]);

      this.app.config(["$locationProvider", function ($locationProvider) {
        // this is required for the location to be able to find queryString parameters.
        $locationProvider.html5Mode({
          enabled: true,
          requireBase: false
        });
      }]);

      _angular2["default"].bootstrap(this.selector, ["SearchSPA"]);
    }

    _createClass(SearchSPA, null, [{
      key: "shared",
      value: function shared(selector, config) {
        this.instance != null ? this.instance : this.instance = new SearchSPA(selector, config);
        return this.instance;
      }
    }]);

    return SearchSPA;
  })();

  module.exports = SearchSPA;
});