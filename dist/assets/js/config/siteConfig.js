define(["exports", "module"], function (exports, module) {
    "use strict";

    var configuration = {
        name: "GEF Generator Master",
        components: {
            "googleTranslate": {
                "selector": "#google_translate_element",
                "script": "utilities/googleTranslate"
            },

            "globalLinkDrop": {
                "selector": ".sws-global-link-button",
                "script": "utilities/globalLinkDrop"
            },

            "ariaToggleMenu": {
                "selector": ".sws-mega-menu",
                "script": "utilities/ariaToggleMenu",
                "config": {
                    "subject": "li.uk-parent.sws-parent-menu"
                }
            },

            "charLengthLong": {
                "selector": ".sws-char-length-100",
                "script": "utilities/charLength"
            },

            "charLengthShort": {
                "selector": ".sws-char-length-43",
                "script": "utilities/charLength"
            },

            "backToTopShow": {
                "selector": "body",
                "script": "utilities/scrollToggle",
                "config": {
                    "checkpoint": 1500,
                    "subject": ".gef-btt",
                    "passed_attr": "data-show",
                    "scroll_window": true
                }
            },

            "backToTopStick": {
                "selector": "html",
                "script": "utilities/scrollToggle",
                "config": {
                    "checkpoint": ".local-footer",
                    "subject": ".gef-btt",
                    "passed_attr": "data-stick",
                    "scroll_window": true
                }
            },

            "mobileNavAccessibility": {
                "selector": ".sws-local-mobile-nav",
                "script": "utilities/mobileNavAccessibility",
                "config": {
                    "hello": "hello"
                }
            },

            "viewMoreNewsletter": {
                "selector": ".sws-newsletter",
                "script": "utilities/viewMoreNewsletter"
            },

            "newsViewMore": {
                "selector": ".news-view-more",
                "script": "utilities/newsViewMore"
            },

            "loadMoreEventLanding": {
                "selector": ".event-view-more",
                "script": "utilities/loadMoreEventLanding"
            },

            "tertiaryNavAccessibility": {
                "selector": "#sws-side-navigation",
                "script": "utilities/tertiaryNavAccessibility"
            },

            "albumDetails": {
                "selector": "#sws-album-details",
                "script": "utilities/albumDetails"
            },

            "albumsLanding": {
                "selector": "#albums-landing-view-more",
                "script": "utilities/loadMoreAlbumsLanding"
            },

            "contactusMap": {
                "selector": "#contact_map_wrapper",
                "script": "utilities/contactusMap"
            },
            "swsIESpec": {
                "selector": "body",
                "script": "utilities/swsIESpec"
            },
            "externalLink": {
                "selector": ".gef-main *:not('.gef-add-this') a, [data-gef-external-link-tagger] a",
                "script": "utilities/externalLinkTagger",
                "config": {
                    "domains": ["localhost"]
                }
            },
            "setTheme": {
                "selector": "body",
                "script": "utilities/setTheme"
            }
        },
        requireConfig: {}

    };

    // Update RequireJS config options
    require.config(configuration.requireConfig);

    module.exports = configuration;
});