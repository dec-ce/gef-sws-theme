define(["exports", "module", "vendor/moment/moment", "vendor/dot/doT"], function (exports, module, _vendorMomentMoment, _vendorDotDoT) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _moment = _interopRequireDefault(_vendorMomentMoment);

  var _doT = _interopRequireDefault(_vendorDotDoT);

  /**
   * Stores social media post data
   *
   * @since 1.1.31
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2015 State Government of NSW 2015
   *
   * @class
   *
   * @requires momment:vendor/moment/moment
   */

  var Post = (function () {

    /**
     * Post constructor
     *
     * @constructor
     *
     * @param {Object} rawPost - The raw social media post returned from the Social Media Aggregator microservice
     * @param {boolean} posttruncate 
     * @example
     * // Instantiate a new Post
     * let Post = new Post(rawPost)
     */

    function Post(rawPost) {
      _classCallCheck(this, Post);

      if (rawPost) {
        this.processRawPost(rawPost);
      }
    }

    /**
     * Exports the Post class as a module
     * @module
     */

    /**
     * Takes a raw social media post returned by the Social Media Aggregator and maps it to Post properties
     *
     * @param {Object} rawPost - The raw social media post returned from the Social Media Aggregator microservice
     */

    _createClass(Post, [{
      key: "processRawPost",
      value: function processRawPost(rawPost) {
        this.rawPost = rawPost;

        this.service = rawPost.accountType;
        this.user = {
          username: rawPost.username,
          name: rawPost.screenName,
          avatar: rawPost.avatarUrl,
          url: rawPost.userUrl
        };
        this.published = rawPost.rawTimestamp;
        this.content = {
          photo: rawPost.thumbnail,
          text: rawPost.text
        };
        this.url = rawPost.url;

        // If the post doesn't already have an ID then in the case of YouTube and Vimeo posts use the video ID as the post ID
        if (rawPost.postId) {
          this.id = rawPost.postId;
        } else if (rawPost.accountType == "vimeo" || rawPost.accountType == "youtube") {
          this.id = this.extractVideoID(rawPost.url);
        }
      }

      /**
       * Takes a Vimeo or YouTube video URL and returns the video ID
       *
       * @param {String} url - A video URL
       *
       * @returns {String} - A YouTube or Vimeo video ID
       */
    }, {
      key: "extractVideoID",
      value: function extractVideoID(url) {
        // If the URL is from YouTube
        if (url.match(/youtube/)) {
          return url.match(/\?v=([^&]*)/)[1];
        }
        // If the URL is from Vimeo
        if (url.match(/vimeo/)) {
          return url.match(/\https:\/\/vimeo.com\/([^&]*)/)[1];
        }
      }

      /**
       * Returns a YouTube/Vimeo iframe embed code compatible URL
       *
       * @returns {String} - The URL needed by the YouTube/Vimeo iframe embed code
       */
    }, {
      key: "embedUrl",
      value: function embedUrl() {
        // If the URL is from YouTube
        if (this.url.match(/youtube/)) {
          return "https://www.youtube.com/embed/" + this.id + "?rel=0";
        }
        // If the URL is from Vimeo
        if (this.url.match(/vimeo/)) {
          return "https://player.vimeo.com/video/" + this.id + "?rel=0";
        }
      }

      /**
       * Return's a Post as HTML string that conforms to the GEF Social Media Post component spec
       *
       * @param {Number} contentLength - (optional) The number of characters the text content of the post should be trimed to
       * @param {Boolean} minimalist - (optional) Set to true to hide the majority of post metadata
       * @param {Boolean} dateplacement - (optional) Set to true to have a differet date design placement
       * 
       * @returns {String} - Post as a HTML conforming to the GEF Social Media Post component
       */
    }, {
      key: "toHTML",
      value: function toHTML(contentLength, minimalist, dateplacement) {
        var post = this;

        // If contentLength is specified then reduce the size of the text content
        if (post.content.text) {
          var posttruncate;
          if (contentLength) {
            var urlwrapinpost = function urlwrapinpost(poststr) {
              return '<a href="' + poststr + '">' + poststr + '</a>';
            }
            //add truncate style only if the post is truncated
            ;

            // If contentLength is specified then reduce the size of the text content
            if (post.content.text.length > contentLength) {
              posttruncate = true;
            }
            post.content.text = post.content.text.substring(0, contentLength);

            //find if text has url if yes then wrap ahref around it
            post.content.text = post.content.text.replace(/\bhttp[^ ]+/ig, urlwrapinpost);
            if (posttruncate) {
              post.content.text = post.content.text.substr(0, Math.min(post.content.text.length, post.content.text.lastIndexOf(" ")));
              post.content.text = post.content.text.replace(/\s*$/, "") + "…<span class=\"show-on-sr\">post truncated</span>";
            }
          }
        }

        if (post.service) {
          if (String.prototype.capitalize = function () {
            return this.charAt(0).toUpperCase() + this.slice(1);
          }) ;
        }
        var templateFn = _doT["default"].template("\n    {{? !it.post.service.match(/error/) }}\n       <li>\n        <article class=\"gef-social-media-post{{? it.minimalist }}--minimalist{{?}} , gef-social-media-post__current-media-{{=it.post.service}}\">\n          {{? !it.minimalist }}\n          <div class=\"gef-social-media-post__metadata\">\n            <div class=\"gef-social-media-post__avatar\">\n              <img src=\"{{=it.post.user.avatar}}\" alt=\"\" />\n            </div>\n            <div class=\"gef-social-media-post__metadata-content\">\n              <p class=\"gef-social-media-post__name\">{{=it.post.user.name}}</p>\n              {{? !it.dateplacement }}\n                {{? it.post.service.match(/twitter/) }}<p class=\"gef-social-media-post__metadatum\">@{{=it.post.user.username}}</p>{{?}}\n              {{?}}\n              {{? it.dateplacement }}\n                <p class=\"gef-social-media-post__metadatum\"><span class=\"show-on-sr\">posted on</span>{{=it.moment(it.post.published).format('D MMM YYYY')}}</p>\n              {{?}}\n            </div>\n            <a href={{=it.post.user.url}} class=\"gef-social-media-post__link-{{=it.post.service}} gef-remove-external-link\" aria-label=\"{{=it.post.user.name}} profile on {{=it.post.service}}\">\n              <i class=\"gef-social-media-post__icon uk-icon-{{=it.post.service}}\" aria-hidden=\"true\" aria-label=\"{{=it.post.service}} icon\"></i></a>\n          </div>\n          {{?}}\n          {{? it.post.content.text}}<section class=\"gef-social-media-post__content\">{{=it.post.content.text}}</section>{{?}}\n          {{? it.post.service.match(/youtube|vimeo/)}}<section class=\"gef-social-media-post__media\">\n            <iframe  width=\"360\" height=\"203\" title=\"{{=it.post.id}}video\" frameborder=\"0\" src=\"{{=it.post.embedUrl()}}\" frameborder=\"0\" allowfullscreen ></iframe>\n          </section>{{?}}\n          {{? it.post.content.photo && !it.post.service.match(/youtube|vimeo/)}}<section class=\"gef-social-media-post__media\">\n          <div class=\"gef-social-media-post__media-imgwrapper\"><img src=\"{{=it.post.content.photo}}\" class=\"gef-social-media-post__media-img-{{=it.post.service}}\" alt=\"\" /></div>\n          </section>{{?}}\n          {{? !it.dateplacement }}\n           <div class=\"gef-social-media-post__metadata\">\n             <p class=\"gef-social-media-post__metadatum\"><span class=\"show-on-sr\">posted</span>{{=it.moment(it.post.published).fromNow()}}</p>\n             {{? it.minimalist}}\n             <i class=\"gef-social-media-post__icon uk-icon-{{=it.post.service}}\" aria-hidden=\"true\" aria-label=\"{{=it.post.service}} icon\"></i>\n             {{?}}\n           </div>\n          {{?}}\n          <div class=\"gef-social-media-post__metadata\">\n            <p class=\"gef-social-media-post__metadata-viewfeed\">\n              <a class=\"gef-social-media-post__metadata-{{=it.post.service}}\" href=\"{{=it.post.url}}\">View post on {{=it.post.service.capitalize()}}<span class=\"gef-external-link\"></span></a>\n            </p>\n  \n        </article>\n      </li> \n    {{?}}");

        var mediaHTML = templateFn({
          post: post,
          moment: _moment["default"],
          minimalist: minimalist,
          dateplacement: dateplacement
        });

        return mediaHTML;
      }

      /**
       * Returns a string representation of the SocialMediaAccount object
       *
       * @returns {String} - Post object as JSON string
       */
    }, {
      key: "toString",
      value: function toString() {
        return JSON.stringify(this);
      }
    }]);

    return Post;
  })();

  module.exports = Post;
});