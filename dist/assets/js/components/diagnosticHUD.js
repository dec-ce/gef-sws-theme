define(["exports", "module", "jquery", "core/notificationCentre", "core/environment"], function (exports, module, _jquery, _coreNotificationCentre, _coreEnvironment) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _NotificationCentre = _interopRequireDefault(_coreNotificationCentre);

  var _Environment = _interopRequireDefault(_coreEnvironment);

  /**
   * The DiagnosticHUD module displays diagnotic information broadcast via the NotificationCentre in a Heads Up Display (HUD)
   *
   * @since 1.1.15
   *
   * @author Digital Services <communications@det.nsw.edu.au>
   * @copyright © 2016 State Government of NSW 2016
   *
   * @class
   * @requires jQuery
   * @requires NotificationCentre
   * @requires Environment
   */

  var DiagnosticHUD = (function () {

    /**
     * Constructor for DiagnosticHUD
     *
     * @constructor
     *
     * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options
     *
     * @example
     * // Instantiate a new DiagnosticHUD
     * let diagnosticHUD = new DiagnosticHUD(".gef-global-header", {})
     */

    function DiagnosticHUD(selector, config) {
      _classCallCheck(this, DiagnosticHUD);

      // Check if only one argument has been passed
      if (arguments.length === 1) {
        // If argument[0] is a config object then set the config arg and nullify the selector arg
        if (selector instanceof Object && !(selector instanceof _jquery)) {
          config = selector;
          selector = null;
        }
      }

      this.$container = (0, _jquery)(selector);

      // Default class config options
      this.config = {};

      // Check if config has been passed to constructor
      if (config) {
        // Merge default config with passed config
        this.config = _jquery.extend(true, {}, this.config, config);
      }

      // Get a reference to the NotificationCentre singleton
      this.notificationCentre = _NotificationCentre["default"].shared();
      this.authenticated = false;

      // Check to see we actually found the component in the DOM
      if (this.$container.length > 0) {

        this.$component = (0, _jquery)("<div class=\"gef-diagnostic-hud\"></div>");
        this.$container.prepend(this.$component);

        // Subscribe to diagnostic events broadcast by other classes
        this.notificationCentre.subscribe("diagnosticUpdate", this.updateHUD.bind(this));
      }
    }

    /**
     * Exports the DiagnosticHUD class as a JS module
     * @module
     */

    /**
     * Construct a new HTML update message
     */

    _createClass(DiagnosticHUD, [{
      key: "buildUpdate",
      value: function buildUpdate(payload) {

        var $update = (0, _jquery)("<div class=\"gef-diagnostic-hud__update\">\n      <span class=\"gef-diagnostic-hud__update-timestamp\">" + new Date().toLocaleTimeString() + "</span>\n      <span class=\"gef-diagnostic-hud__update-module\">" + payload.module + "</span> \n    </div>");

        for (var i = 0; i < payload.messages.length; i++) {

          var message = payload.messages[i];
          var textColour = "gef-diagnostic-hud__text-info";

          switch (message.variable) {
            case true:
              textColour = "gef-diagnostic-hud__text-success";
              break;
            case false:
              textColour = "gef-diagnostic-hud__text-danger";
              break;
            default:
              textColour = "gef-diagnostic-hud__text-info";
              break;
          }

          $update.append((0, _jquery)("<span class=\"gef-diagnostic-hud__update-message\">" + message.text + (message.hasOwnProperty("variable") ? ": <span class=\"" + textColour + "\">" + message.variable + "</span>" : '') + "</span>"));
        }

        return $update;
      }

      /**
       * Update the HUD
       */
    }, {
      key: "updateHUD",
      value: function updateHUD(payload) {
        this.$component.prepend(this.buildUpdate(payload));
      }

      /**
       * Static function to instatiate the DiagnosticHUD class as singleton
       *
       * @static
       *
       * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
       * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
       *
       * @returns {DiagnosticHUD} Reference to the same DiagnosticHUD instatiated in memory
       */
    }], [{
      key: "shared",
      value: function shared(selector, config) {
        return this.instance != null ? this.instance : this.instance = new DiagnosticHUD(selector, config);
      }
    }]);

    return DiagnosticHUD;
  })();

  module.exports = DiagnosticHUD;
});