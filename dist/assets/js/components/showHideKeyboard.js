define(["exports", "module", "keycodes", "uikit!accordion", "jquery"], function (exports, module, _keycodes, _uikitAccordion, _jquery) {
  "use strict";

  var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  var _KeyCodes = _interopRequireDefault(_keycodes);

  var _UI = _interopRequireDefault(_uikitAccordion);

  var ShowHideKeyboard = (function () {

    /**
     * @constructor
     *
     * @param {string} selector - CSS selector or jQuery object of HTML element to use
     * @param {object} config   - class configuration arguments
     */

    function ShowHideKeyboard(selector, config) {
      _classCallCheck(this, ShowHideKeyboard);

      this.init(selector);
    }

    /**
     * AccordionKeyboard updating accordion element attribute for accessibility
     *
     * @module components/showHideKeyboard
     */

    /**
    * Initiate a show hide item - so can be used externally
    */

    _createClass(ShowHideKeyboard, [{
      key: "init",
      value: function init(selector) {
        this.$selector = (0, _jquery)(selector);
        // NOTE: Had to switch away from listening to the UIKit custom event "update.uk.accordion" becuase it didnt' seem to fire reliably when the DOM content dynamically changed
        // @todo See if there is a way to only inititate the keybaord support if we know the UIKit accordion has finished loading
        // this.$selector.on("update.uk.accordion", this.applyKeyboardSupport.bind(this))
        this.applyKeyboardSupport(null, this.$selector);
      }

      /**
       * Add keyboard and ARIA support to UIKit accordions
       *
       * @param {Event} event - The event triggered by UIKit
       * @param {jQuery} $accordion - UIKit spec compliant wrapper element for an accordion
       */
    }, {
      key: "applyKeyboardSupport",
      value: function applyKeyboardSupport(event, $accordion) {
        var _self = this;
        var $accordionTitles = $accordion.find(".uk-accordion-title");

        // Loop through all accordion titles
        $accordionTitles.each(function (index, title) {
          var $accordionTitle = (0, _jquery)(title);
          // Get ID for controlled DIV
          var containerID = $accordionTitle.attr("aria-controls");

          // Toggle attributes on click events
          // _self.toggleAttributes($accordionTitle, containerID)
          _self.toggleAttributes($accordion);

          // Mimic click event on keydown
          _self.toggleKeydown($accordionTitle);

          // Set attributes for corresponding
          _self.setAttributes($accordionTitle, containerID);
        });
      }

      /**
       * Toggle attributes on keydown
       *
       * @param {jQuery} $accordionTitle -
       *
       * @returns {jQuery} The accordion title element
       */
    }, {
      key: "toggleKeydown",
      value: function toggleKeydown($accordionTitle) {
        // Trigger accordion title click event on keydown
        $accordionTitle.on("keydown", function (event) {
          event.stopImmediatePropagation();
          if (event.which === _KeyCodes["default"]["return"] || event.keyCode === _KeyCodes["default"]["return"]) {
            $accordionTitle.click();
          } else {
            $accordionTitle.next("div").focus();
          }
        });

        return $accordionTitle;
      }

      /**
       * Toggle accessibility attributes
       *
       * @param {jQuery} $accordionTitle -
       * @param {jQuery} containerID -
       *
       * @returns {jQuery} The accordion title element
       */
      // toggleAttributes($accordionTitle, containerID) {
    }, {
      key: "toggleAttributes",
      value: function toggleAttributes($accordion, containerID) {

        // On accordion item toggle
        $accordion.on("toggle.uk.accordion", function (event, active, toggle, content) {
          if (active) {
            // If it is update accessiblity attributes for expanded state
            toggle.attr({ "aria-expanded": "true" });
            toggle.attr({ "aria-selected": "true" });
            content.parent().attr({ "tabindex": "0", "aria-hidden": "false" });
          } else {
            // If it is update accessiblity attributes for collapsed state
            toggle.attr({ "aria-expanded": "false" });
            toggle.attr({ "aria-selected": "false" });
            content.parent().attr({ "tabindex": "-1", "aria-hidden": "true" });
          }
        });
      }

      /**
       * Attach attributes to accordion content blocks
       *
       * @param {jQuery} $accordionTitle -
       * @param {jQuery} containerID -
       *
       * @returns {jQuery} The accordion title element
       */
    }, {
      key: "setAttributes",
      value: function setAttributes($accordionTitle, containerID) {

        this.timeout = setTimeout(function () {
          $accordionTitle.next("div").attr({
            "id": containerID,
            "aria-labelledby": $accordionTitle.attr("id"),
            "role": "tabpanel",
            "tabindex": "-1",
            "aria-hidden": "true"
          });
          return $accordionTitle;
        }, 50);
        // Add accessiblity attributes to accordion content wrapper generated by uikit
      }
    }]);

    return ShowHideKeyboard;
  })();

  module.exports = ShowHideKeyboard;
});