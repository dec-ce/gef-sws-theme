"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function () {
    return gulp.src(options.paths.dev.assets.json)
      .pipe(gulp.dest(options.paths.dist.assets.json))
      .on("error", errorHandler)
  }
}
