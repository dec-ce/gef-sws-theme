"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv, browserSync) {
  return function() {

    return gulp.src([options.paths.dev.assets.less])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.less({
        paths: "bower_components"
      }))

      // Append any missing vendor prefixes for CSS3 properties
      .pipe(plugins.autoprefixer({
          browsers: ["last 2 versions", "IE 9", "IE 10"],
          cascade: false
      }))

      // if flag --cms=matrix change background image sources
      .pipe(plugins.if((argv.cms === "matrix"), plugins.replace("../images/", "mysource_files/" )))

      // If the --compress flag is true then compress the CSS
      .pipe(plugins.if(options.compress, plugins.cssnano({ autoprefixer: false })))

      // Write the CSS files to disk
      .pipe(gulp.dest(options.paths.dist.assets.css))

      // Inject CSS if not distribute task
      .pipe(plugins.if(!options.distribute, browserSync.stream()))
  }
}
