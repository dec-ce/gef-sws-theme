"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug) {
  let destinations = options.themes

  return function() {

    destinations.forEach(function(theme) {

      console.log(theme)

      let pipeline = gulp.src(options.paths.dev.templates)

      pipeline = pipeline.pipe(plugins.plumber({
        errorHandler: errorHandler
      }))


      pipeline = pipeline.pipe(plugins.pug({
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/",
          theme: theme
        }
      }))

      pipeline = pipeline.pipe(gulp.dest("dist/" + theme))

    })

    // return pipeline

  }
}
