"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv, browserSync, lessImport) {
  return function() {
    return gulp.src([options.paths.dev.assets.lessConcatinate])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))
      .pipe(plugins.less())
      .pipe(plugins.rename(function (path) {
         path.extname = ".less"
      }))
      .pipe(gulp.dest(options.paths.dist.assets.less))
  }
}
