"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug, currentItem) {
  console.log("pug-templtes theme v2")
  
  function buildThemes(theme) {
    let destFolder = options.paths.dist.root
    if (theme != options.paths.dist.root) {
      destFolder += theme + "/"
    }
    console.log(theme, destFolder)
    // return function() {
      gulp.src(options.paths.dev.templates)
        // Stop gulp from crashing on error
        .pipe(plugins.plumber({
          errorHandler: errorHandler
        }))

        // use gulp-cached to only run pug on templates that have changed.
        .pipe(plugins.cached('pug-templates'))

        // pug
        .pipe(plugins.pug({
          pretty: true,
          data: {
            revision: options.revision,
            configPath: "../js/"
          }
        }))

        .pipe(gulp.dest(destFolder))
        //.end()
        
  //  }
  }

  return function() {
    for (let i=0; i<options.themes.length; i++) {
      let theme = options.themes[i]
      buildThemes(theme)
    }
  }

}
