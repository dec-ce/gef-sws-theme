"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv, browserSync) {
  return function() {

    /**
     * Returns an array of file paths (strings) to the built HTML pages for each theme in the dist folder
     */
    function getBuiltHTMLPaths() {
      var htmlPaths = new Array()

      // Loop through each theme
      for (var i = 0; i < options.themes.length; i++) {
        // Loop through each HTML file in the UnCSS connfig stored in our global options
        for (var x = 0; x < options.deployment.uncss.html.length; x++) {
          htmlPaths.push(options.paths.dist.root + options.themes[i] + "/" + options.deployment.uncss.html[x])
        }
      }
      
      return htmlPaths
    }


    return gulp.src([options.paths.dev.assets.sass])
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.sass({
        includePaths: "bower_components",
        outputStyle: options.compress ? "compressed" : "expanded"
      }))

      // Append any missing vendor prefixes for CSS3 properties
      .pipe(plugins.autoprefixer({
        browsers: ["last 2 versions", "IE 9", "IE 10"],
        cascade: false
      }))

      // if flag --cms=matrix change background image sources
      .pipe(plugins.if((argv.cms === "matrix"), plugins.replace("../images/", "mysource_files/" )))

      // Uncss
      .pipe(plugins.if(options.uncss, plugins.uncss({
        html: getBuiltHTMLPaths(),
        ignore: options.deployment.uncss.ignore
      })))

      // If the --compress flag is true then compress the CSS
      .pipe(plugins.if(options.compress, plugins.cssnano({autoprefixer: false})))

      // Write the CSS files to disk
      .pipe(gulp.dest(options.paths.dist.assets.css))

      // Inject CSS if not distribute task
      .pipe(plugins.if(!options.distribute, browserSync.stream()))
  }
}
