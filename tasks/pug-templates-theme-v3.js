"use strict"

module.exports = function(gulp, plugins, options, errorHandler, pug) {
  let destinations = options.themes

  return function() {

    let pipeline =
    gulp.src(options.paths.dev.templates)

      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      // use gulp-cached to only run pug on templates that have changed.
      .pipe(plugins.cached('pug-templates'))

      // pug
      .pipe(plugins.pug({
        pretty: true,
        data: {
          revision: options.revision,
          configPath: "../js/",
          themeName: theme
        }
      }))
    destinations.forEach(function(d) {
      console.log("theme", d)
      pipeline = pipeline.pipe(gulp.dest("dist/" + d))
    })

  }
}
